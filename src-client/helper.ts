/**
 * HELPER
 * Provides useful functions that might be shared among components.
 */

import { Page } from './app-interfaces';

// analyses the current URL to determine what the state of the app should be
export function analyseURL() {
    let url = window.location.pathname;
    if (url.startsWith('/user/')) {
        let handle = url.substring(url.indexOf('/', 1) + 1);
        let idx = handle.indexOf('/');

        if (idx === -1) {
            return {
                page:Page.PROFILE,
                handle:handle
            }
        } else {
            let action = handle.substring(idx + 1);

            if (action === 'followers') {
                return {
                    page:Page.FOLLOWERS,
                    handle:handle.substring(0, idx)
                };
            }
        }
    } else if (url.startsWith('/search/')) {
        let handle = url.substring(url.indexOf('/', 1) + 1).replace(/%23/g, '#');
        return {
            page:Page.SEARCH,
            handle
        };
    }

    return {
        page:Page.HOME
    };
}