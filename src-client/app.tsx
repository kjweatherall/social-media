/**
 * APP
 * Main components that controls the site.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { Page } from './app-interfaces';
import { MainState } from './app-interfaces';

// actions
import { setPage, setAccount } from './redux/actions/account-actions';

// components
import Nav from './components/nav';
import Home from './pages/home/home';
import Profile from './pages/profile/profile';
import Search from './pages/search/search';
import Followers from './pages/followers/followers';
import SearchBox from './components/search-box';

// functions
import { analyseURL } from './helper';
import { BACKGROUND_COLOR } from './styles';

/* REDUX */

const mapState = (state: MainState)=>({
    viewing: state.accountData.viewing,
    account: state.accountData.account,
});

const mapDispatch = {
    setPage,
    setAccount
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/* APP */
const App = class extends React.Component<Props> {
    timer: NodeJS.Timeout = null;

    constructor(props:Props) {
        super(props);

        this.onNavigation = this.onNavigation.bind(this);
    }

    componentDidMount() {
        window.addEventListener('popstate', this.onNavigation);
        this.timer = setInterval(this.checkedLoggedIn.bind(this), 60000);
    }

    componentWillUnmount() {
        window.removeEventListener('popstate', this.onNavigation);
        if (this.timer) {
            clearInterval(this.timer);
            this.timer = null;
        }
    }

    componentDidUpdate(prevProps: Props) {
        // TODO: Confirm that this is necessary
        if (this.props.viewing.page !== prevProps.viewing.page) {
            window.scrollTo(0, 0);
        }
    }

    checkedLoggedIn() {
        fetch('/api/v1/users')
        .then(res=>{
            if (res.status !== 200) {
                window.location.href = '/login';
            }
        })
        .catch(err=>{
            console.error(`Error checking logged in: ${err}`);
            window.location.href = '/login';
        })
    }

    // when browser navigates back or forward
    onNavigation() {
        let page = analyseURL();

        switch (page.page) {
            case Page.HOME:
                return this.props.setPage(Page.HOME, null, true);
            case Page.PROFILE:
                return this.props.setPage(Page.PROFILE, page.handle, true);
            case Page.SEARCH:
                return this.props.setPage(Page.SEARCH, page.handle, true);
            case Page.FOLLOWERS:
                return this.props.setPage(Page.FOLLOWERS, page.handle, true);
        }
    }

    // gets component for current page
    getMainPage() {
        switch (this.props.viewing.page) {
            case Page.HOME:
                return <Home />;
            case Page.PROFILE:
                return <Profile />;
            case Page.SEARCH:
                return <Search />;
            case Page.FOLLOWERS:
                return <Followers />;
            default:
                return null;
        }
    }

    render() {
        return (
            <div style={{textAlign:'center',marginTop:'-5px'}}>
                <div style={{display:'flex',minHeight:'100vh'}}>
                    <div style={{flex:2}}>
                        <Nav />
                    </div>
                    <div style={{flex:3,display:'grid',gridTemplateColumns:'600px auto'}}>
                        <div style={{width:'600px',position:'relative',borderLeft:'1px solid rgb(56, 68, 77)',borderRight:'1px solid rgb(56, 68, 77)'}}>
                            {this.getMainPage()}
                        </div>
                        {this.props.viewing.page !== Page.SEARCH && 
                            <div style={{textAlign:'left'}}>
                                <SearchBox />
                            </div>
                        }
                    </div>                    
                </div>
            </div>
        );
    }
};

export default connector(App);

document.body.style.backgroundColor = BACKGROUND_COLOR;