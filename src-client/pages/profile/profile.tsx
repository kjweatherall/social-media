/**
 * PROFILE
 * A component for displaying a user's details and their posts.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { Profile } from '../../../shared/SharedInterfaces';
import { Page, MainState } from '../../app-interfaces';

// actions
import { setPage, setProfile, followProfile, unfollowProfile } from '../../redux/actions/account-actions';
import { Actions } from '../../redux/actions/posts-actions';

// components
import PageComp from '../page';
import { ProfileImageLarge } from '../../components/profile-image';
import FollowButton from '../../components/follow-button';
import ProfileEditor from './components/profile-editor';
import ImageViewer from '../../components/image-viewer';

interface ProfileState {
    showingEditor: boolean;
    viewing: string;
}

/* REDUX */
let mapState = (state:MainState)=>({
    viewing:state.accountData.viewing,
    profile:state.accountData.profile
});

let mapDispatch = {
    setPage,
    setProfile,
    followProfile,
    unfollowProfile
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/**
 * HEADER
 * Component for area at top of profile page.
 */
function Header(props: {onBackPressed:()=>void,user:Profile}) {
    return (
        <div style={{
            width:'100%',
            backgroundColor:'rgb(21, 32, 43)',
            borderBottom:'1px solid rgb(56, 68, 77)',
            height:'50px',
            color:'white',
            fontFamily:'system-ui',
            fontWeight:800,
            textAlign:'left',
            fontSize:'19px',
            display:'grid',
            gridTemplateColumns:'75px auto'
        }}>
        <div style={{textAlign:'center',cursor:'pointer'}} onClick={props.onBackPressed}><i style={{marginTop:'15px'}} className="fas fa-arrow-left"></i></div>
        {props.user ?
            <div>
                <div>{props.user.displayName}</div>
                <div style={{fontSize:'13px'}}>{props.user.posts} posts</div>
            </div> :
            <div style={{marginTop:'10px'}}>User not found</div>
        }
        </div>
    );
}

/* PROFILE */
let Profile = class extends React.Component<Props, ProfileState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            showingEditor:false,
            viewing:''
        };
    }

    componentDidMount() {
        if (this.props.viewing) {
            if (!this.props.profile || this.props.profile.handle !== this.props.viewing.handle) {
                this.fetchProfile();
            }
        }
    }

    componentDidUpdate(prevProps: Props) {
        if (this.props.viewing && (!prevProps.viewing || (prevProps.viewing.handle !== this.props.viewing.handle))) {
            this.fetchProfile();
        }
    }

    fetchProfile() {
        if (this.props.viewing) {
            this.props.setProfile(this.props.viewing.handle);
        }
    }

    onBackPressed() {
        this.props.setPage(Page.HOME);
    }

    onFollowButtonPressed() {
        switch (this.props.profile.connection) {
            case 'self':
                this.setState({showingEditor:true});
                break;
            case 'following':
                this.props.unfollowProfile(this.props.profile.handle);
                break;
            case 'unconnected':
                this.props.followProfile(this.props.profile.handle);
                break;
        }
    }

    render() {
        return (
            <PageComp 
                header={<Header onBackPressed={this.onBackPressed.bind(this)} user={this.props.profile}/>}
                getPostURL={(handle:string)=>`/api/v1/users/${handle}/posts`}    
            >
                {this.props.profile&& 
                    <div style={{width:'100%',position:'relative',marginBottom:'5px',borderBottom:'1px solid rgb(56, 68, 77)'}}>
                        <img 
                            src={this.props.profile.bannerImageThumbnail ? `/images/thumbnails/banners/${this.props.profile.bannerImageThumbnail}` : this.props.profile.bannerImage ? `/images/banners/${this.props.profile.bannerImage}` : '/banner.jpg'} 
                            style={{width:'100%',height:'200px',objectFit:"cover",cursor:'pointer'}} 
                            onClick={()=>this.setState({viewing:this.props.profile.bannerImage ? `/images/banners/${this.props.profile.bannerImage}` : '/banner.jpg'})} 
                        />
                        <ProfileImageLarge
                            profileImage={this.props.profile.profileImage}
                            thumbnailImage={this.props.profile.profileImageThumbnailLarge}
                            style={{position:'absolute',left:'10px',top:'120px'}}
                            onClick={()=>this.setState({viewing:this.props.profile.profileImage ? `/images/profiles/${this.props.profile.profileImage}` : '/profile.jpg'})}
                        />
                        <div style={{height:'50px',padding:'5px'}}>
                            <FollowButton
                                profile={this.props.profile}
                                onClicked={this.onFollowButtonPressed.bind(this)}
                            />
                        </div>
                        <div style={{padding:'0px 20px',textAlign:'left'}}>
                            <div style={{textAlign:'left',fontSize:'19px',fontWeight:800,color:'white',fontFamily:'system-ui'}}>{this.props.profile.displayName}</div>
                            <div style={{color:'lightgrey',height:'25px'}}>@{this.props.profile.handle}</div>
                            <pre style={{color:'white',fontFamily:'system-ui',fontSize:'19px',margin:'5px 0'}}>{this.props.profile.bio}</pre>
                            <div style={{fontSize:'19px',color:'lightgrey',textAlign:'left',cursor:'pointer'}} onClick={()=>this.props.setPage(Page.FOLLOWERS, this.props.viewing.handle)}>
                                <span style={{color:'white',fontWeight:800}}>{this.props.profile.following}</span> following <span style={{color:'white',fontWeight:800,marginLeft:'20px'}}>{this.props.profile.followers}</span> followers
                            </div>
                        </div>
                    </div>
                }
                {this.state.showingEditor&&
                    <ProfileEditor
                        close={()=>this.setState({showingEditor:false})}
                    />
                }
                {this.state.viewing&&
                    <ImageViewer
                        url={this.state.viewing}
                        close={()=>this.setState({viewing:''})}
                    />
                }
            </PageComp>
        );
    }
};

export default connector(Profile);