/**
 * PROFILE EDITOR
 * A component that provides a panel to edit your profile.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { MainState } from '../../../app-interfaces';
import { Profile } from 'interfaces';

// actions
import { setBanner, setProfilePhoto, updateProfile } from '../../../redux/actions/account-actions';
import { ProfileImageLarge } from '../../../components/profile-image';

/**
 * close: A function that, when called, will hide this component.
 */
interface ProfileEditorProps {
    close():void;
}

/**
 * name: Current value of the name.
 * bio: Current value for the bio.
 */
interface ProfileEditorState {
    name: string;
    bio: string;
}

/* REDUX */

const mapState = (state:MainState)=>({
    profile:state.accountData.profile
});

const mapDispatch = {
    setBanner,
    setProfilePhoto,
    updateProfile
};

const connector = connect(mapState, mapDispatch);

type Props = ProfileEditorProps & ConnectedProps<typeof connector>;

/* PROFILE EDITOR */
let ProfileEditor = class extends React.Component<Props, ProfileEditorState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            name:props.profile.displayName || '',
            bio:props.profile.bio || ''
        }
    }

    // save button pressed
    onSave() {
        // check if name or bio changed
        let nameChanged = this.props.profile.displayName !== this.state.name,
            bioChanged = this.props.profile.bio !== this.state.bio;

        if (!nameChanged && !bioChanged) {
            // do nothing if name and bio are both the same
            this.props.close();
        } else {
            // collect updates to send with request
            let updates: Partial<Profile> = { };

            if (nameChanged) {
                updates.displayName = this.state.name;
            }
            if (bioChanged) {
                updates.bio = this.state.bio;
            }

            // send update request
            fetch(`/api/v1/users/${this.props.profile.handle}`, {method:'put',headers:{'content-type':'application/json'},body:JSON.stringify(updates)})
            .then(res=>{
                if (res.status === 204) {
                    // apply updates to profile data in state
                    this.props.updateProfile(updates);
                    // close panel
                    this.props.close();
                } else {
                    throw `Got response status ${res.status}`;
                }
            })
            .catch(alert);
        }
    }

    // user pushed "change banner" button
    changeBanner() {
        // add hidden file input to page
        let fileUpload = document.createElement('input');
        fileUpload.setAttribute('type', 'file');        
        document.body.appendChild(fileUpload);
        fileUpload.style.display = 'none';

        // add listener to check when user has selected an image
        fileUpload.addEventListener('change', ()=>{
            this.props.setBanner(this.props.profile.handle, fileUpload.files);
            fileUpload.remove();
        });

        // remove element if operation was cancelled 
        fileUpload.addEventListener('cancel', ()=>fileUpload.remove());

        // trigger "choose file" dialog
        fileUpload.click();
    }

    // user pusged "change profile image" button
    changeProfilePhoto() {
        // add hidden file input to page
        let fileUpload = document.createElement('input');
        fileUpload.setAttribute('type', 'file');        
        document.body.appendChild(fileUpload);
        fileUpload.style.display = 'none';

        // add listener to check when user has selected an image
        fileUpload.addEventListener('change', ()=>{
            this.props.setProfilePhoto(this.props.profile.handle, fileUpload.files);
            fileUpload.remove();
        });

        // remove element if operation was cancelled
        fileUpload.addEventListener('cancel', ()=>fileUpload.remove());

        // trigger "choose file" dialog
        fileUpload.click();
    }

    render() {
        return (
            <div style={{position:'fixed',width:'100%',height:'100%',left:0,top:0,backgroundColor:'rgba(110, 118, 125, 0.4)',display:'grid',justifyContent:'center',alignContent:'center',zIndex:2}} onClick={this.props.close}>
                <div style={{position:'relative',width:'600px',backgroundColor:'rgb(21, 32, 43)',borderRadius:'15px'}} onClick={evt=>evt.stopPropagation()}>
                    <div style={{display:'grid',gridTemplateColumns:'50px auto 75px',padding:'15px'}}>
                        <div style={{color:'white',fontSize:'19px',cursor:'pointer'}} onClick={this.props.close}>X</div>
                        <div style={{fontFamily:'system-ui',fontWeight:800, fontSize:'19px',color:'white',textAlign:'left'}}>Edit profile</div>
                        <div>
                            <input type="button" value="Save" style={{fontSize:'19px',fontWeight:600,color:'white',border:'none',backgroundColor:'rgb(29, 161, 242)',borderRadius:'50px',padding:'10px 15px'}} onClick={this.onSave.bind(this)} />
                        </div>                        
                    </div>
                    <div style={{height:'550px',maxHeight:'90vh'}}>
                        <div style={{width:'100%',height:'200px',position:'relative'}}>
                            <img style={{width:'100%',height:'100%',objectFit:'cover'}} src={this.props.profile.bannerImage ? `/images/banners/${this.props.profile.bannerImage}` : '/banner.jpg'} />
                            <div style={{position:'absolute',display:'grid',justifyContent:'center',alignContent:'center',left:0,top:0,width:'100%',height:'100%'}}>
                                <input type='button' value='+' onClick={this.changeBanner.bind(this)} />
                            </div>
                        </div>
                        <div style={{marginLeft:'10px',marginRight:'10px'}}>
                            <div style={{position:'relative',width:'112px',height:'112px',marginTop:'-45px'}}>
                                <ProfileImageLarge
                                    profileImage={this.props.profile.profileImage}
                                    thumbnailImage={this.props.profile.profileImageThumbnailLarge}
                                    style={{width:'100%',height:'100%'}}
                                />
                                <div style={{position:'absolute',display:'grid',justifyContent:'center',alignContent:'center',left:0,top:0,width:'100%',height:'100%'}}>
                                    <input type='button' value='+' onClick={this.changeProfilePhoto.bind(this)} />
                                </div>
                            </div>
                            <div style={{backgroundColor:'rgb(25, 39, 52)',textAlign:'left',borderBottom:'1px solid rgb(136, 153, 166)', marginTop:'10px'}}>
                                <div style={{color:'lightgrey'}}>Name</div>
                                <input type='text' value={this.state.name} onChange={e=>this.setState({name:e.target.value})} style={{color:'white',border:'none',backgroundColor:'transparent',fontSize:'19px',width:'100%'}} />
                            </div>
                            <div style={{backgroundColor:'rgb(25, 39, 52)',textAlign:'left',borderBottom:'1px solid rgb(136, 153, 166)', marginTop:'10px'}}>
                                <div style={{color:'lightgrey'}}>Bio</div>
                                <textarea value={this.state.bio} onChange={e=>this.setState({bio:e.target.value})} style={{color:'white',border:'none',backgroundColor:'transparent',fontSize:'19px',width:'100%',height:'85px',overflowY:'auto'}} />
                            </div>
                        </div>
                    </div>                    
                </div>
            </div>
        );
    }
};

export default connector(ProfileEditor);