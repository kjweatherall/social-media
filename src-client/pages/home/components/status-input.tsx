/**
 * STATUS INPUT
 * A component to allow the user to make a post.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { MainState, Page } from '../../../app-interfaces';

// actions
import { setPage } from '../../../redux/actions/account-actions';
import { Actions } from '../../../redux/actions/posts-actions';

// components
import { ProfileImageStandard } from '../../../components/profile-image';

/**
 * status: Text entered by user.
 * filesChosen: If files have been chosen for uploading.
 */
interface StatusInputState {
    status: string;
    filesChosen: boolean;
    error: string;
    postButtonDisabled: boolean;
}

/* REDUX */

let mapState = (state:MainState)=>({
    account:state.accountData.account,
    viewing:state.accountData.viewing
});

let mapDispatch = {
    fetchPosts:Actions.fetchPosts,
    addPost:Actions.addPost,
    setPage
};

let connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/* STAUS INPUT */
const StatusInput = class extends React.Component<Props, StatusInputState> {
    filesRef: React.RefObject<HTMLInputElement>;
    
    constructor(props: Props) {
        super(props);

        this.state = {
            status:'',
            filesChosen:false,
            error:'',
            postButtonDisabled:false
        };

        // keep a reference to the file input so the files chosen by the user
        // can be retrieved when uploading
        this.filesRef = React.createRef();
    }

    // check if files have been chosen to upload
    onFilesChanged() {
        this.setState({filesChosen:this.filesRef.current.files.length > 0});
    }

    // send post to server
    makePost() {
        // do nothing if neither text nor files are provided
        if (this.state.status === '' && !this.state.filesChosen) {
            return;
        }

        // disable controls while sending
        this.setState({postButtonDisabled:true});
        if (this.filesRef.current) {
            this.filesRef.current.setAttribute('disabled', 'disabled');
        }

        // if files provided, get them
        if (this.filesRef.current && this.filesRef.current.files.length > 0) {
            var files = this.filesRef.current.files;
        }

        // send post for request to server
        this.props.addPost(this.state.status, this.props.account.handle, files, (err)=>{
            if (!err) {
                // clear text/files when post was successful
                this.setState({
                    status:'',
                    filesChosen:false,
                    error:'',
                    postButtonDisabled:false
                });

                if (this.filesRef.current) {
                    this.filesRef.current.value = null;
                    this.filesRef.current.removeAttribute('disabled');
                }
            } else {
                this.setState({
                    error:err,
                    postButtonDisabled:false
                });

                if (this.filesRef.current) {
                    this.filesRef.current.removeAttribute('disabled');
                }
            }
        });
    }

    // for checking when enter is pressed
    onKeyPressed(evt: React.KeyboardEvent) {
        if (evt.key === 'Enter') {
            this.makePost();
        }
    }

    render() {
        if (!this.props.account) {
            return null;
        }

        return (
            <div style={{display:'grid',gridTemplateColumns:'75px auto', borderBottom:'10px solid rgb(37, 51, 65)'}}>
                <ProfileImageStandard
                    profileImage={this.props.account ? this.props.account.profileImage : ''}
                    thumbnailImage={this.props.account.profileImageThumbnailSmall}
                    onClick={()=>this.props.setPage(Page.PROFILE, this.props.account.handle)}
                />
                <div>
                    <div>
                        <input 
                            type="text"
                            style={{
                                border:'none',
                                fontSize:'19px',
                                width:'100%',
                                backgroundColor:'transparent',
                                color:'white'
                            }} 
                            placeholder="What's happening?"
                            value={this.state.status}
                            onChange={e=>this.setState({status:e.target.value})}
                            onKeyPress={this.onKeyPressed.bind(this)}
                        />
                    </div>
                    <div style={{marginTop:'5px',marginBottom:'5px'}}>                        
                        <div style={{textAlign:'right'}}>
                            <input 
                                type="file"
                                ref={this.filesRef}
                                style={{
                                    color:'white'
                                }}
                                onChange={this.onFilesChanged.bind(this)}
                            />
                            <input 
                                type="button" 
                                value="Post"
                                style={{
                                    backgroundColor:'rgb(29, 161, 242)',
                                    fontSize:'15px',
                                    borderRadius:'9999px',
                                    border:'none',
                                    color:'white',
                                    paddingTop:'10px',
                                    paddingBottom:'10px',
                                    paddingLeft:'20px',
                                    paddingRight:'20px',
                                    fontFamily:'system-ui',
                                    fontWeight:600,
                                    opacity:((this.state.status||this.state.filesChosen)&&!this.state.postButtonDisabled)?1:0.5
                                }}
                                disabled={this.state.postButtonDisabled}
                                onClick={this.makePost.bind(this)}
                            />
                        </div>
                        {this.state.error&&<div style={{color:'red'}}>{this.state.error}</div>}
                    </div>
                </div>
            </div>
        );
    }
}

export default connector(StatusInput);