/**
 * HOME
 * The home page. Displays a stream of the user's posts mixed with the posts of those they follow.
 */
import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { MainState } from 'src-client/app-interfaces';

// actions
import { Actions } from '../../redux/actions/posts-actions';

// components
import Page from '../page';
import StatusInput from './components/status-input';

/**
 * HEADER
 * Header at the top of the Home page.
 */
function Header() {
    return (
        <div style={{
            width:'100%',
            backgroundColor:'rgb(21, 32, 43)',
            borderBottom:'1px solid rgb(56, 68, 77)',
            height:'53px',
            color:'white',
            fontFamily:'system-ui',
            fontWeight:800,
            textAlign:'left',
            fontSize:'19px',
            lineHeight:'50px'
        }}>
            Home
        </div>
    );
}

/* REDUX */
let mapState = (state:MainState)=>({
    account:state.accountData.account,
    viewing:state.accountData.viewing
});

let mapDispatch = {
    fetchPosts:Actions.fetchPosts,
    addPost:Actions.addPost
};

let connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

interface HomeState {
    status: string;
    filesChosen: boolean;
}

/* HOME */
const Home = class extends React.Component<Props, HomeState> {
    render() {
        return (
            <Page
                header={<Header />}
                getPostURL={()=>`/api/v1/wall`}
            >
                <StatusInput />
            </Page>
        );
    }
};

export default connector(Home);