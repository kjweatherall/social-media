/**
 * FOLLOWERS
 * Component for displaying the users a user is following and followed by.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { FollowersResults } from '../../../shared/SharedInterfaces';
import { Page, MainState } from '../../app-interfaces';
import { TEXT_COLOR, TEXT_COLOR_SECONDARY, MENU_SELECTED } from '../../styles';

// actions
import { setPage, setProfile, followProfile, unfollowProfile } from '../../redux/actions/account-actions';
import { Actions } from '../../redux/actions/posts-actions';

// components
import ProfileCard from '../../components/profile-card';

/**
 * showingFollowers: Shows followers if true, otherwise shows following.
 * data: Collection of users following and being followed.
 */
interface FollowersState {
    showingFollowers: boolean;
    data: FollowersResults;
}

/* REDUX */

let mapState = (state:MainState)=>({
    viewing:state.accountData.viewing,
    profile:state.accountData.profile
});

let mapDispatch = {
    setPage,
    setProfile,
    followProfile,
    unfollowProfile,
    fetchPosts:Actions.fetchPosts
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/* FOLLOWERS */
let Followers = class extends React.Component<Props, FollowersState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            showingFollowers: true,
            data:{
                followers:[],
                following:[]
            }
        };

        this.onFollowClicked = this.onFollowClicked.bind(this);
        this.onProfileClicked = this.onProfileClicked.bind(this);
    }

    componentDidMount() {
        if (!this.props.profile || this.props.viewing.handle !== this.props.profile.handle) {
            // correct profile not loaded; load it
            this.props.setProfile(this.props.viewing.handle);
        } else {
            // get followers for profile
            this.fetchFollowers();
        }
    }

    // update follower data if profile changes
    componentDidUpdate(prevProps: Props) {
        // get followers if profile changed
        if (this.props.profile) {
            if (!prevProps.profile || (this.props.profile.handle !== prevProps.profile.handle)) {
                this.fetchFollowers();
            }
        }
    }

    // gets follower data
    fetchFollowers() {
        fetch(`/api/v1/users/${this.props.viewing.handle}/followers`)
        .then(res=>{
            if (res.status === 200) {
                return res.json();
            } else {
                throw `Got status ${res.status}`;
            }
        })
        .then((results:FollowersResults)=>this.setState({data:results}))
        .catch(alert);
    }

    // back button pressed
    // TODO: Should this actually use browser's "back" function?
    onBackPressed() {
        this.props.setPage(Page.PROFILE, this.props.viewing.handle);
    }

    // button clicked to show followers
    onFollowersClicked() {
        this.setState({showingFollowers:true});
    }

    // button clicked to show following
    onFollowingClicked() {
        this.setState({showingFollowers:false});
    }

    // a user's profile was clicked
    onProfileClicked(handle: string) {
        this.props.setPage(Page.PROFILE, handle);
    }

    // button to follow a user was clicked
    onFollowClicked(handle: string, action: 'follow'|'unfollow') {
        // callback for when response from server is received
        let callback = ()=>{
            let data = this.state.data;

            // update connection with followers
            for (let f of data.followers) {
                if (f.handle === handle) {
                    f.connection = (action === 'follow' ? 'following' : 'unconnected');
                    break;
                }
            }

            // update connection with followed
            for (let f of data.following) {
                if (f.handle === handle) {
                    f.connection = (action === 'follow' ? 'following' : 'unconnected');
                    break;
                }
            }

            this.setState({data:data});
        };
        
        // send appropriate request to server
        switch (action) {
            case 'follow':
                return this.props.followProfile(handle, callback);
            case 'unfollow':
                return this.props.unfollowProfile(handle, callback);
        }
    }

    render() {
        return (
            <div style={{color:TEXT_COLOR}}>
                <div style={{position:'sticky',top:0,display:'grid',gridTemplateColumns:'100px auto'}}>
                    <div style={{textAlign:'center',cursor:'pointer'}} onClick={this.onBackPressed.bind(this)}>
                        <i style={{marginTop:'15px',color:TEXT_COLOR}} className="fas fa-arrow-left"></i>
                    </div>                    
                    <div style={{textAlign:'left'}}>
                        <div style={{fontSize:'19px'}}>{this.props.profile ? this.props.profile.displayName : 'Profile Not Found'}</div>
                        {this.props.profile&&<div style={{color:TEXT_COLOR_SECONDARY}}>@{this.props.profile.handle}</div>}
                    </div>
                </div>
                {this.props.profile&&
                    <div>
                        <div style={{display:'grid',gridTemplateColumns:'50% 50%',fontSize:'19px',fontWeight:600,textAlign:'center',borderBottom:'1px solid rgb(56, 68, 77)'}}>
                            <div style={{cursor:'pointer',borderBottom:this.state.showingFollowers?'1px solid skyblue':'none',color:this.state.showingFollowers?MENU_SELECTED:TEXT_COLOR,padding:'10px'}} onClick={this.onFollowersClicked.bind(this)}>Followers</div>
                            <div style={{cursor:'pointer',borderBottom:!this.state.showingFollowers?'1px solid skyblue':'none',color:!this.state.showingFollowers?MENU_SELECTED:TEXT_COLOR,padding:'10px'}} onClick={this.onFollowingClicked.bind(this)}>Following</div>
                        </div>
                        {(this.state.showingFollowers?this.state.data.followers:this.state.data.following).map(f=>(
                            <ProfileCard
                                key={f.handle}
                                onFollowClicked={this.onFollowClicked}
                                onProfileClicked={this.onProfileClicked}
                                profile={f}
                            />
                        ))}
                    </div>
                }
            </div>
        );
    }
};

export default connector(Followers);