/**
 * PAGE
 * A component that consists of a stream of content with an optional fixed-position header.
 */
import * as React from 'react';

// components
import Stream from './stream';

/**
 * header: Optional header above stream.
 * getPostURL: Gets URL to get content for stream;
 */
interface StreamPageProps {
    header?: JSX.Element;

    getPostURL(handle: string): string;
}

const StreamPage = class extends React.Component<StreamPageProps> {
    render() {
        return (
            <div>
                {this.props.header&&
                    <div style={{position:'sticky',top:'0px',zIndex:2}}>
                        {this.props.header}
                    </div>
                }
                <div style={{marginTop:'20px'}}>
                    {this.props.children}
                    <Stream 
                        getPostURL={this.props.getPostURL}
                    />
                </div>
            </div>
        );
    }
};

export default StreamPage;