/**
 * STREAM
 * A component for displaying a stream of content. Automatically fetches newer content and older content when
 * end is scrolled to.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { MainState, Page } from '../app-interfaces';
import { COLOR_LIGHT } from '../styles';

// actions
import { Actions } from '../redux/actions/posts-actions';
import { followProfile, unfollowProfile, setPage } from '../redux/actions/account-actions';

// components
import Post from '../components/post';
import ProfileCard from '../components/profile-card';

interface StreamProps {
    getPostURL(handle: string):string;
}

/**
 * now: Time used to calculate time since a post was made.
 */
interface StreamState {
    now: number;
}

/* REDUX */
let mapState = (state:MainState)=>({
    posts:state.postsData.posts,
    profiles:state.postsData.profiles,
    hasMore:state.postsData.hasMore,
    newPosts:state.postsData.newPosts.length,
    viewing:(state.accountData.viewing),
    handle:state.accountData.account ? state.accountData.account.handle : ''
});

let mapDispatch = {
    fetchPosts:Actions.fetchPosts,
    fetchOlderPosts:Actions.fetchOlderPosts,
    fetchNewPosts:Actions.fetchNewPosts,
    showNewPosts:Actions.showNewPosts,
    followProfile,
    unfollowProfile,
    setPage
};

const connector = connect(mapState, mapDispatch);

type Props = StreamProps & ConnectedProps<typeof connector>;

/* STREAM */
let Stream = class extends React.Component<Props, StreamState> {
    // interval for checking for new posts
    newPostTimer: NodeJS.Timeout = null;
    // interval for updating "time since posts" on posts
    timeTimer: NodeJS.Timeout = null;
    // if older posts are being fetched
    fetchingMore: boolean = false;

    constructor(props: Props) {
        super(props);

        this.state = {
            now: new Date().getTime()
        };

        this.onScroll = this.onScroll.bind(this);
    }

    componentDidMount() {
        window.addEventListener('scroll', this.onScroll);

        this.newPostTimer = setInterval(this.checkForNew.bind(this), 10000);
        this.timeTimer = setInterval(this.onTime.bind(this), 60000);

        // get content
        let url = this.props.getPostURL(this.props.viewing.handle);
        if (url) {
            this.props.fetchPosts(url);
        }
    }

    componentWillUnmount() {
        window.removeEventListener('scroll', this.onScroll);

        clearInterval(this.newPostTimer);
        clearInterval(this.timeTimer);
    }

    componentDidUpdate(prevProps: Props) {
        // fetch content if handle/search changed
        if (prevProps.viewing.handle !== this.props.viewing.handle) {
            this.fetchingMore = false;

            let url = this.props.getPostURL(this.props.viewing.handle);
            if (url) {
                this.props.fetchPosts(url);
            }
        }
    }

    // updates "time since posted" time for posts
    onTime() {
        this.setState({now:new Date().getTime()});
    }

    // gets newer posts
    checkForNew() {
        let time = (this.props.posts.length > 0 ? this.props.posts[0].addedAt : 0);
        let url = this.props.getPostURL(this.props.viewing ? this.props.viewing.handle : null);
        if (url) {
            this.props.fetchNewPosts(url + `${url.includes('?')?'&':'?'}after=${time}`);
        }
    }

    // fetch new content if user scrolls near bottom of pahe
    onScroll() {
        if (!this.fetchingMore && this.props.hasMore && window.pageYOffset >= document.body.scrollHeight - window.innerHeight - 50) {
            this.loadMore();
        }
    }

    // gets next set of older posts
    loadMore() {
        // getting "older" posts depends on having at least 1 post
        if (this.props.posts.length === 0) {
            return;
        }

        let url = this.props.getPostURL(this.props.viewing ? this.props.viewing.handle : null);
        if (url) {
            let time = this.props.posts[this.props.posts.length - 1].addedAt;

            this.fetchingMore = true;
            this.props.fetchOlderPosts(url + `${url.includes('?')?'&':'?'}before=${time}`, ()=>{
                this.fetchingMore = false;
            });            
        }
    }

    // follow button was clicked for a user
    onFollowClicked(handle: string, action: 'follow'|'unfollow') {
        switch (action) {
            case 'follow':
                return this.props.followProfile(handle);
            case 'unfollow':
                return this.props.unfollowProfile(handle);
        }
    }

    render() {
        return (
            <div style={{marginBottom:'30px'}}>
                {this.props.profiles.length > 0 &&
                    <div style={{marginTop:'3px',borderTop:`1px solid ${COLOR_LIGHT}`,borderBottom:`5px solid ${COLOR_LIGHT}`}}>
                        <h3 style={{margin:'3px 10px',color:'white',textAlign:'left'}}>People</h3>
                        {this.props.profiles.map(p=>(
                            <div key={p.handle} style={{borderTop:`1px solid ${COLOR_LIGHT}`}}>
                                <ProfileCard
                                    profile={p}
                                    onFollowClicked={this.onFollowClicked.bind(this)}
                                    onProfileClicked={handle=>this.props.setPage(Page.PROFILE, handle)}
                                />
                            </div>
                        ))}
                    </div>
                }
                {this.props.posts.map(p=>(
                    <Post
                        key={p.streamID}
                        mine={this.props.handle === p.handle}
                        post={p}
                        now={this.state.now}
                    />
                ))}
            </div>
        );
    }
}

export default connector(Stream);