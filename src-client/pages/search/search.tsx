/**
 * SEARCH
 * A component for displaying search results.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { Page } from '../../app-interfaces';
import { MainState } from '../../app-interfaces';

// actions
import { setPage } from '../../redux/actions/account-actions';

// components
import PageComp from '../page';
import SearchBox from '../../components/search-box';

/* REDUX */
const mapState = (state:MainState)=>({
    account:state.accountData.account,
    search:state.accountData.viewing.handle
});

const mapDispatch = {
    setPage:setPage
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/* SEARCH */
const Search = class extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    goBack() {
        this.props.setPage(Page.HOME);
    }

    render() {
        return (
            <PageComp
                getPostURL={handle=>`/api/v1/search?query=${handle.replace(/#/g,'%23')}`}
            >
                <div>
                    <span style={{float:'left',cursor:'pointer',paddingLeft:'10px'}} onClick={this.goBack.bind(this)}><i className="fas fa-arrow-left" style={{color:'white'}}></i></span>
                    <SearchBox />
                </div>
            </PageComp>
        );
    }
};

export default connector(Search);