/**
 * PROFILE IMAGE
 * A convenient component that provides a profile image with the standard style.
 */

import * as React from 'react';

const ProfileImageStyle: React.CSSProperties = {
    width:'100%',
    height:'100%',
    objectFit:'cover',
    borderRadius:'50%'
};

interface ProfileImageProps {
    profileImage: string;
    thumbnailImage: string;
    style?: React.CSSProperties;
    
    onClick?():void;
}

/**
 * PROFILE IMAGE
 * Base component.
 */
const ProfileImage = (props: ProfileImageProps)=>{
    let style = Object.assign({}, ProfileImageStyle, {cursor:props.onClick?'pointer':''}, props.style);
    
    if (props.thumbnailImage) {
        var url = `/images/thumbnails/profiles/${props.thumbnailImage}`;
    } else if (props.profileImage) {
        var url = `/images/profiles/${props.profileImage}`;
    } else {
        var url = '/profile.jpg';
    }

    return <img src={url} style={style} onClick={props.onClick} />;
};

/**
 * PROFILE IMAGE CLICKABLE
 * Provides a small profile image that can be clicked.
 */
export const ProfileImageStandard = (props: ProfileImageProps)=>{
    let style = Object.assign({cursor:'pointer',width:'55px',height:'55px'}, props.style);

    return (
        <ProfileImage
            profileImage={props.profileImage}
            thumbnailImage={props.thumbnailImage}
            style={style}
            onClick={props.onClick}
        />
    );
};

/**
 * PROFILE IMAGE LARGE
 * Provides a profile image that is larger and cannot be clicked.
 */
export const ProfileImageLarge = (props: ProfileImageProps)=>{
    let style = Object.assign({border:'5px solid rgb(21, 32, 43)',width:'132px',height:'132px'}, props.style)
    return (
        <ProfileImage
            profileImage={props.profileImage}
            thumbnailImage={props.thumbnailImage}
            style={style}
            onClick={props.onClick}
        />
    );
}