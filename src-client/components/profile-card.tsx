/**
 * PROFILE CARD
 * Provides an element that contains a profile's image, handle, bio and a follow button.
 */

import * as React from 'react';

// types
import { Profile } from '../../shared/SharedInterfaces';
import { TEXT_COLOR, TEXT_COLOR_SECONDARY } from '../styles';

// components
import { ProfileImageStandard } from './profile-image';
import FollowButton from './follow-button';

interface ProfileCardProps {
    profile: Profile;

    onProfileClicked(handle:string):void;
    onFollowClicked(handle:string, action: 'follow'|'unfollow'):void;
}

export default (props: ProfileCardProps)=>(
    <div style={{display:'grid',gridTemplateColumns:'60px auto 150px',textAlign:'left',borderBottom:'1px solid rgb(56, 68, 77)',padding:'15px 10px',cursor:'pointer'}} onClick={()=>props.onProfileClicked(props.profile.handle)}>
        <ProfileImageStandard
            profileImage={props.profile.profileImage}
            thumbnailImage={props.profile.profileImageThumbnailSmall}
            onClick={()=>props.onProfileClicked(props.profile.handle)}
        />
        <div style={{fontSize:'15px'}}>
            <div style={{color:TEXT_COLOR,fontWeight:700}}>{props.profile.displayName}</div>
            <div style={{color:TEXT_COLOR_SECONDARY}}>@{props.profile.handle}</div>
            <div style={{color:TEXT_COLOR}}>{props.profile.bio}</div>
        </div>
        <div>
            {props.profile.connection !== 'self' &&
                <FollowButton
                    profile={props.profile}
                    onClicked={(evt:React.MouseEvent)=>{
                        evt.stopPropagation();
                        props.onFollowClicked(props.profile.handle, props.profile.connection === 'following' ? 'unfollow' : 'follow');
                    }}
                />
            }
        </div>
    </div>
);