/**
 * POST
 * Content posted by users.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { MainState } from '../app-interfaces';
import { Post } from 'interfaces';
import { Page } from '../app-interfaces';
import { TEXT_COLOR, TEXT_COLOR_LINK, TEXT_COLOR_DELETE, COLOR_DARK, TEXT_COLOR_SECONDARY, COLOR_LIKED, COLOR_UNLIKED, COLOR_LIGHT } from '../styles';

// actions
import { setPage, followProfile, unfollowProfile } from '../redux/actions/account-actions';
import { Actions } from '../redux/actions/posts-actions';

// components
import { ProfileImageStandard } from './profile-image';

/**
 * POST TEXT
 * Text of a post. Used to turn hashtags into links
 */
interface PostTextProps {
    text: string;
    onTagClicked(tag:string):void;
    onAtClicked(handle:string):void;
}

function PostText(props: PostTextProps) {    
    let parts:{value:string,type:'tag'|'at'|'text'}[] = [ ];    
    let prev = 0, prevStr = 0, idx;

    // TODO: Consider processing text when it's submitted to the server, so then it doesn't always been to be processed

    // find hashtags and ats
    do {
        idx = props.text.substring(prev).search(/[@#]/);
        if (idx !== -1) {
            idx += prev;

            if (prevStr !== idx + prev) {
                parts.push({
                    type:'text',
                    value:props.text.substring(prevStr, idx)
                });
            }

            let end = props.text.substring(idx + 1).search(/[^a-zA-Z-_]/);

            if (end !== -1) {
                end += idx + 1;

                if (end - idx > 1) {
                    parts.push({
                        type:props.text[idx][0] === '#' ? 'tag' : 'at',
                        value:props.text.substring(idx, end)
                    });

                    prev = prevStr = end;
                } else {
                    prev += end;
                }
            } else {
                parts.push({
                    type:props.text[idx][0] === '#' ? 'tag' : 'at',
                    value:props.text.substring(idx)
                });

                idx = -1;
                prevStr = props.text.length - 1;
            }
        }
    } while (idx !== -1);

    // add any remaining text after the final hashtag/at
    if (prevStr < props.text.length - 1) {
        parts.push({
            type:'text',
            value:props.text.substring(prevStr)
        });
    }

    return (
        <div style={{color:TEXT_COLOR}}>{parts.map((p,i)=>(
            p.type === 'tag'?
                <span key={i} style={{color:TEXT_COLOR_LINK,textDecoration:'underline',cursor:'pointer'}} onClick={()=>props.onTagClicked(p.value)}>{p.value}</span> :
            p.type === 'at' ?
                <span key={i} style={{color:TEXT_COLOR_LINK,textDecoration:'underline',cursor:'pointer'}} onClick={()=>props.onAtClicked(p.value.substring(1))}>{p.value}</span> :
                <React.Fragment key={i}>{p.value}</React.Fragment>
        ))}</div>
    );
}

/**
 * DROP-DOWN MENU
 * Drop-down menu for actions related to posts.
 */

interface DropDownMenuProps {
    showingMenu: boolean;
    post: Post;
    mine: boolean;
    
    toggleMenu(evt: React.MouseEvent):void;
    deletePost(postID: number):void;
    toggleFollow(handle: string):void;
}

function DropDownMenu(props: DropDownMenuProps) {
    return (
        <div style={{position:'absolute',right:'5px',top:'-5px',zIndex:1}} id={`menu-${props.post.postID}`}>
            <i className="fas fa-caret-square-down" style={{cursor:'pointer'}} onClick={props.toggleMenu}></i>
            {props.showingMenu&&
                <div style={{position:'absolute',right:'3px',marginTop:'-3px',padding:'5px',backgroundColor:COLOR_DARK,borderRadius:'5px',boxShadow:'rgba(136, 153, 166, 0.2) 0px 0px 15px 0px, rgba(136, 153, 166, 0.15) 0px 0px 3px 1px'}}>
                    {!props.mine&&<div>
                        <input type='button' value={(props.post.following ? 'Unfollow' : 'Follow') + ` ${props.post.displayName}`} style={{color:TEXT_COLOR,border:'none',backgroundColor:'transparent'}} onClick={()=>props.toggleFollow(props.post.handle)} />
                    </div>}
                    {props.mine&&(
                        !props.post.repost ?
                            <div className='dark-button'>
                                <input type='button' value='Delete Post' style={{color:TEXT_COLOR_DELETE,border:'none',backgroundColor:'transparent'}} onClick={()=>props.deletePost(props.post.streamID)} />
                            </div> :
                            <div>
                                <input type='button' value='Un-repost' style={{color:TEXT_COLOR,border:'none',backgroundColor:'transparent'}} onClick={()=>props.deletePost(props.post.streamID)} />
                            </div>
                    )}
                </div>
            }
        </div>
    );
}

/**
 * TIME LABEL
 * A span that displays the amount of time since a specified time.
 */

interface TimeLabelProps {
    publishedAt: number;
    now?: number;
}

const MINUTE = 60;
const HOUR = 3600;
const DAY = 86400;
const YEAR = 30672000;

const TimeLabel = (props:TimeLabelProps)=>{
    let now = props.now || new Date().getTime();
    let time = ((now - props.publishedAt) / 1000) >>> 0;

    let timeFrame = time > 1 ? 'seconds' : 'seconds';
    let t = time;

    if (time > YEAR) {
        t = (time / YEAR) >>> 0;
        timeFrame = t > 1 ? 'years' : 'year';
    } else if (time > DAY) {
        t = (time / DAY) >>> 0;
        timeFrame = t > 1 ? 'days' : 'day';
    } else if (time > HOUR) {
        t = (time / HOUR) >>> 0;
        timeFrame = t > 1 ? 'hours' : 'hour';
    } else if (time > MINUTE) {
        t = (time / MINUTE) >>> 0;
        timeFrame = t > 1 ? 'minutes' : 'minute';
    }

    return <span style={{color:TEXT_COLOR_SECONDARY}}>{t} {timeFrame} ago</span>;
}

/**
 * POST
 * The post.
 */

interface PostCompProps {
    post: Post;
    // showComments: boolean;
    mine: boolean;

    now?: number;
}

interface PostCompState {
    showingMenu: boolean;
    viewing: string;
    showingComments: boolean;
}

const mapState = (state: MainState)=>({

});

const mapDispatch = {
    setPage:setPage,
    deletePost:Actions.deletePost,
    likePost:Actions.likePost,
    unlikePost:Actions.unlikePost,
    repost:Actions.repostPost,
    unrepost:Actions.unrepostPost,
    followProfile,
    unfollowProfile
};

const connector = connect(mapState, mapDispatch);

type Props = PostCompProps & ConnectedProps<typeof connector>;

const PostComp = class extends React.Component<Props, PostCompState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            showingMenu:false,
            viewing:'',
            showingComments:false
        };

        this.onWindowClicked = this.onWindowClicked.bind(this);
    }

    componentWillUnmount() {
        window.removeEventListener('click', this.onWindowClicked);
    }

    toggleMenu() {
        if (!this.state.showingMenu) {
            window.addEventListener('click', this.onWindowClicked);
        } else {
            window.removeEventListener('click', this.onWindowClicked);
        }

        this.setState({showingMenu:!this.state.showingMenu});
    }

    toggleLikePost() {
        if (this.props.post.liked) {
            this.props.unlikePost(this.props.post.postID);
        } else {
            this.props.likePost(this.props.post.postID);
        }
    }

    toggleRepost() {
        if (this.props.post.reposted) {
            this.props.unrepost(this.props.post.postID);
        } else {
            this.props.repost(this.props.post.postID);
        }
    }

    /**
     * Called when the window is clicked. Used when post menu is open to close it if something outside the post is being clicked.
     * @param evt Event
     */
    onWindowClicked(evt: Event) {
        let target = evt.target as HTMLElement;

        if (target.id !== `menu-${this.props.post.postID}` && target.parentElement.id !== `menu-${this.props.post.postID}`) {
            this.toggleMenu();
        }
    }

    toggleFollow(handle: string) {
        if (this.props.post.following) {
            this.props.unfollowProfile(handle);
        } else {
            this.props.followProfile(handle);
        }
    }

    render() {
        let thumbnails = this.props.post.thumbnails ? this.props.post.thumbnails.split(',') : [ ];

        return (
            <div style={{position:'relative',paddingTop:'3px',marginTop:'3px',borderBottom:'1px solid rgb(56, 68, 77)',padding:'5px'}}>
                <DropDownMenu
                    post={this.props.post}
                    showingMenu={this.state.showingMenu}
                    toggleMenu={this.toggleMenu.bind(this)}
                    deletePost={this.props.deletePost}
                    toggleFollow={this.toggleFollow.bind(this)}
                    mine={this.props.mine}
                />
                {(this.props.post.repost && this.props.post.repostedByHandle)&&
                    <div style={{cursor:'pointer',fontSize:'13px',color:TEXT_COLOR_SECONDARY,textAlign:'left',paddingLeft:'50px'}} onClick={()=>this.props.setPage(Page.PROFILE, this.props.post.repostedByHandle)}><i className="fas fa-retweet"></i> Reposted by {this.props.post.repostedByName}</div>
                }
                <div key={this.props.post.postID} style={{display:'grid',gridTemplateColumns:'60px auto'}}>                
                    <div style={{paddingRight:'5px'}}>
                        <ProfileImageStandard
                            profileImage={this.props.post.profileImage}
                            thumbnailImage={this.props.post.profileImageThumbnail}
                            onClick={()=>this.props.setPage(Page.PROFILE, this.props.post.handle)}
                        />
                    </div>
                    <div style={{textAlign:'left'}}>
                        <div>
                            <span style={{cursor:'pointer'}} onClick={()=>this.props.setPage(Page.PROFILE, this.props.post.handle)}>
                                <span style={{color:TEXT_COLOR,textDecoration:'none',fontFamily:'system-ui',fontSize:'15px',fontWeight:700,fontStretch:'100%'}} >{this.props.post.displayName}</span>
                                <span style={{paddingLeft:'5px',paddingRight:'5px', color:TEXT_COLOR_SECONDARY,fontSize:'15px',fontWeight:400}}>
                                    @{this.props.post.handle}
                                </span>
                            </span>
                            <span style={{paddingLeft:'5px',paddingRight:'5px', color:TEXT_COLOR_SECONDARY,fontSize:'15px',fontWeight:400}}>·</span>
                            <TimeLabel publishedAt={this.props.post.publishedAt} />
                        </div>
                        <div style={{color:TEXT_COLOR,fontFamily:'system-ui, -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Ubuntu, "Helvetica Neue", sans-serif',fontSize:'15px',fontWeight:400}}>
                            <PostText
                                text={this.props.post.text}
                                onTagClicked={tag=>this.props.setPage(Page.SEARCH, tag)}
                                onAtClicked={handle=>this.props.setPage(Page.PROFILE, handle)}
                            />
                        </div>
                        {this.props.post.images&&
                            this.props.post.images.split(',').map((i, idx)=>(
                                <div key={i} style={{width:'507px',height:'285px',position:'relative',margin:'3px 0px'}}>
                                    <img
                                        src={thumbnails[idx] ? `/images/thumbnails/${thumbnails[idx]}` : `/images/uploads/${i}`}
                                        style={{width:'100%',height:'100%',objectFit:'cover',position:'absolute',top:0,borderRadius:'20px',border:'1px solid rgb(56, 68, 77)'}}
                                        onClick={()=>this.setState({viewing:i})}
                                    />
                                </div>
                            ))
                        }
                        <div style={{textAlign:'right'}}>
                            <div style={{display:'inline-block',color:this.props.post.reposted ? COLOR_LIKED : COLOR_UNLIKED,cursor:'pointer',width:'50px',textAlign:'left'}} onClick={this.toggleRepost.bind(this)}>
                                <i className="fas fa-retweet"></i>
                            </div>
                            <div style={{display:'inline-block',color:this.props.post.liked ? COLOR_LIKED : COLOR_UNLIKED,cursor:'pointer',width:'50px',textAlign:'left'}} onClick={this.toggleLikePost.bind(this)}>
                                <i className="fas fa-thumbs-up"></i>
                                {this.props.post.likes > 0 && <span> {this.props.post.likes}</span>}
                            </div>
                        </div>
                    </div>
                </div>
                {this.state.viewing !== '' &&
                    <div 
                        style={{position:'fixed',left:0,top:0,width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,.7)',display:'grid',justifyContent:'center',alignContent:'center',zIndex:2}}
                        onClick={()=>this.setState({viewing:''})}
                    >
                        <img src={`/images/uploads/${this.state.viewing}`} style={{maxWidth:'95vw',maxHeight:'95vh'}} />
                    </div>
                }
            </div>
        );
    }
};

export default connector(PostComp);