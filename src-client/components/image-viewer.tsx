import * as React from 'react';

interface ImageViewerProps {
    url: string;
    close(): void;
}

export default (props: ImageViewerProps)=>(
    <div 
        style={{position:'fixed',left:0,top:0,width:'100%',height:'100%',backgroundColor:'rgba(0,0,0,.7)',display:'grid',justifyContent:'center',alignContent:'center',zIndex:2}}
        onClick={props.close}
    >
        <img src={props.url} style={{maxWidth:'95vw',maxHeight:'95vh'}} />
    </div>
);