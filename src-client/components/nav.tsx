/**
 * NAV
 * Nav bar on the side of the page.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { Page, MainState } from '../app-interfaces';
import { TEXT_COLOR } from '../styles';

// actions
import { setPage } from '../redux/actions/account-actions';
import { ProfileImageStandard } from './profile-image';

/* REDUX */
const mapState = (state:MainState)=>({
    account:state.accountData.account
});

const mapDispatch = {
    setPage:setPage
};

let connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/**
 * ICON
 * Simple element for displaying an icon.
 */

interface IconProps {
    icon: string;
}

const Icon = (props: IconProps)=>{
    return <i className={`fas fa-${props.icon}`}></i>
};

/**
 * NAV BUTTON
 * Button in the nav bar.
 */
interface NavButtonProps {
    icon: JSX.Element;
    text: string;
    onClick(): void;
}

const NavButton = (props: NavButtonProps)=>{
    return (
        <li style={{margin:'10px 0'}}>
            <div style={{display:'grid',gridTemplateColumns:'40px auto',height:'40px',cursor:'pointer',marginRight:'5px'}} onClick={props.onClick}>
                <div style={{textAlign:'center'}}>{props.icon}</div>
                <div>{props.text}</div>
            </div>
        </li>
    );
};

/* NAV */
const Nav = class extends React.Component<Props> {
    constructor(props: Props) {
        super(props);
    }

    render() {
        return (
            <nav style={{color:TEXT_COLOR,fontSize:'19px',fontWeight:600,fontFamily:'system-ui',float:'right',width:'235px',position:'sticky',top:0}}>
                <ul style={{listStyle:'none',textAlign:'left'}}>
                    <NavButton
                        icon={<Icon icon='home' />}
                        text='Home'
                        onClick={()=>this.props.setPage(Page.HOME)}
                    />
                    {/* <NavButton
                        icon={<Icon icon='comment-alt' />}
                        text='Notifications'
                        onClick={()=>{}}
                    />
                    <NavButton
                        icon={<Icon icon='envelope' />}
                        text='Messages'
                        onClick={()=>{}}
                    /> */}
                    <NavButton
                        icon={
                            <div style={{margin:'0 auto'}}>
                                {this.props.account&&
                                    <ProfileImageStandard
                                        profileImage={this.props.account.profileImage}
                                        thumbnailImage={this.props.account.profileImageThumbnailSmall}
                                        style={{width:'35px',height:'35px'}}
                                    />
                                }
                            </div>
                        }
                        text='Profile'
                        onClick={()=>this.props.setPage(Page.PROFILE, this.props.account.handle)}
                    />
                    <NavButton
                        icon={<Icon icon='sign-out-alt' />}
                        text='Logout'
                        onClick={()=>window.location.href='/logout'}
                    />
                </ul>
            </nav>
        );
    }
};

export default connector(Nav);