/**
 * FOLLOW BUTTON
 * Button for following/unfollowing someone. Also provides "edit profile" when used with your own account.
 */

import * as React from 'react';

// types
import { Profile } from '../../shared/SharedInterfaces';
import { BUTTON_COLOR } from '../styles';

interface FollowButtonProps {
    profile: Profile;
    includeEditProfile?: boolean;

    onClicked(evt: React.MouseEvent):void;
}

export default (props: FollowButtonProps)=>{
    // do nothing when profile if your own and profile editing isn't available
    if (props.profile.connection === 'self' && props.includeEditProfile === false) {
        return null;
    }

    // determine text of button
    switch (props.profile.connection) {
        case 'self':
            var text = 'Edit profile';
            break;
        case 'following':
            var text = 'Unfollow';
            break;
        case 'unconnected':
            var text = 'Follow';
            break;
    }

    return (
        <input 
            type='button' 
            value={text} 
            style={{backgroundColor:BUTTON_COLOR,border:'none',float:'right',color:'white',padding:'10px 20px',fontFamily:'system-ui',fontWeight:600,borderRadius:'50px',fontSize:'19px',cursor:'pointer'}} 
            onClick={props.onClicked} 
        />
    );
};