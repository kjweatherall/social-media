/**
 * SEARCH BOX
 * Provides a text box with an autocomplete drop-down box for searching people and posts.
 */

import * as React from 'react';
import { connect, ConnectedProps } from 'react-redux';

// types
import { AutocompleteResults, ProfileBasic } from 'interfaces';
import { Page, MainState } from '../app-interfaces';
import { TEXT_COLOR, TEXT_COLOR_SECONDARY, SEARCH_BOX, AUTOCOMPLETE_SELECTED, COLOR_DARK } from '../styles';

// actions
import { setPage } from '../redux/actions/account-actions';

// components
import { ProfileImageStandard } from './profile-image';

/**
 * PROFILE RESULT
 * Component for displaying a profile in autocomplete box.
 */

 /**
  * profile: Profile being displayed.
  * selected: If item is current selected via arrow keys.
  * onProfileSelected: When profile is clicked.
  */
interface ProfileResultProps {
    profile: ProfileBasic;
    selected: boolean;
    onProfileClicked(handle: string): void;
}

const ProfileResult = (props: ProfileResultProps)=>{
    return (
        <div style={{display:'grid',gridTemplateColumns:'75px auto',cursor:'pointer',backgroundColor:props.selected?'green':'transparent',marginBottom:'5px'}} onClick={()=>props.onProfileClicked(props.profile.handle)}>
            <ProfileImageStandard
                profileImage={props.profile.profileImage}
                thumbnailImage={props.profile.profileImageThumbnailSmall}
                onClick={()=>props.onProfileClicked(props.profile.handle)}
            />
            <div style={{textAlign:'left'}}>
                <div style={{color:TEXT_COLOR,fontSize:'15px'}} >{props.profile.displayName}</div>
                <div style={{color:TEXT_COLOR_SECONDARY,fontSize:'15px'}} >@{props.profile.handle}</div>
            </div>
        </div>
    );
}

/**
 * search: Current value of text box.
 * searchResults: Autocomplete results returned from server.
 * idx: Index of autocomplete item selected using arrow keys.
 */
interface SearchBoxState {
    search: string;
    autocompleteResults: AutocompleteResults;
    idx: number;
}

/* REDUX */
const mapState = (state:MainState)=>({
    search:state.accountData.search
});

const mapDispatch = {
    setPage:setPage
};

let connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector>;

/* SEARCH BOX */
const SearchBox = class extends React.Component<Props, SearchBoxState> {
    constructor(props: Props) {
        super(props);

        this.state = {
            search: this.props.search || '',
            autocompleteResults: {
                accounts:[],
                hashtags:[]
            },
            idx:-1
        };
        
        this.onProfileClicked = this.onProfileClicked.bind(this);
    }

    componentDidUpdate(prevProps: Props) {
        if (prevProps.search !== this.props.search) {
            this.setState({search:this.props.search});
        }
    }

    reset() {
        this.setState({
            idx:-1,
            search:'',
            autocompleteResults:{
                accounts:[],
                hashtags:[]
            }
        });
    }

    performAutocomplete() {
        // replace hashtag to make appropriate URL
        let search = this.state.search.replace(/#/g,'%23');

        fetch(`/api/v1/autocomplete?query=${search}`)
        .then(res=>{
            if (res.status === 200) {
                return res.json();
            } else {
                throw `Status code ${res.status}`;
            }
        })
        .then((autocompleteResults:AutocompleteResults)=>{
            this.setState({
                autocompleteResults,
                idx:Math.min(this.state.idx,autocompleteResults.accounts.length - 1)
            });
        })
        .catch(err=>alert(`Error searching: ${err}`));
    }

    onChange(evt: React.ChangeEvent<HTMLInputElement>) {
        let search = evt.target.value;
        if (search) {
            this.setState({search:search}, this.performAutocomplete.bind(this));
        } else {
            this.reset();
        }
    }

    onProfileClicked(handle: string) {
        this.setState({search:'',autocompleteResults:{accounts:[],hashtags:[]},idx:-1});
        this.props.setPage(Page.PROFILE, handle);
    }

    onKeyPress(evt: React.KeyboardEvent) {
        if (evt.key === 'ArrowUp') {
            if (this.state.idx > -1) {
                this.setState({idx:this.state.idx - 1});
            }
        } else if (evt.key === 'ArrowDown') {
            if (this.state.idx < this.state.autocompleteResults.accounts.length + this.state.autocompleteResults.hashtags.length - 1) {
                this.setState({idx:this.state.idx + 1});
            }
        } else if (evt.key === 'Enter') {
            let idx = this.state.idx;

            if (idx > -1) {
                // an item was selected; activate appropropriate response
                this.setState({search:'',autocompleteResults:{accounts:[],hashtags:[]},idx:-1});
                if (idx < this.state.autocompleteResults.accounts.length) {
                    this.props.setPage(Page.PROFILE, this.state.autocompleteResults.accounts[this.state.idx].handle);
                } else {
                    // this.props.setSearch(`#${this.state.autocompleteResults.hashtags[idx - this.state.autocompleteResults.accounts.length]}`);
                    this.props.setPage(Page.SEARCH, `#${this.state.autocompleteResults.hashtags[idx - this.state.autocompleteResults.accounts.length]}`);
                }
            } else if (this.state.search !== '') {
                // no item was selected; just use standard search
                this.showFullResults(this.state.search);
            }
        }
    }

    // moves to search page for full search results
    showFullResults(search: string) {
        // this.props.setSearch(search);
        this.props.setPage(Page.SEARCH, search);
        this.reset();
    }

    render() {
        return (
            <div style={{position:'sticky',top:'7px',display:'inline-block',zIndex:1,color:TEXT_COLOR}}>
                <input 
                    type='text'
                    style={{color:TEXT_COLOR, backgroundColor:SEARCH_BOX,fontSize:'15px',borderRadius:'20px',marginLeft:'10px',padding:'15px',border:'1px solid rgb(29, 161, 242)',userSelect:'none',outline:'none',width:'250px'}} 
                    onChange={this.onChange.bind(this)}
                    value={this.state.search}
                    onKeyDown={this.onKeyPress.bind(this)}
                />
                {(this.state.autocompleteResults.accounts.length + this.state.autocompleteResults.hashtags.length > 0) &&
                    <div style={{position:'absolute',top:'50px',left:'25px',width:'250px',height:'350px',overflowY:'auto',border:'1px solid rgb(29, 161, 242)',backgroundColor:COLOR_DARK}}>
                        {this.state.autocompleteResults.accounts.length > 0 && 
                            <div>
                                <h3>Users</h3>
                                {this.state.autocompleteResults.accounts.map((r,i)=>(
                                    <ProfileResult
                                        key={r.handle}
                                        profile={r}
                                        selected={i === this.state.idx}
                                        onProfileClicked={this.onProfileClicked}
                                    />
                                ))}
                            </div>
                        }
                        {this.state.autocompleteResults.hashtags.length > 0 &&
                            <div>
                                <h3>Hashtags</h3>
                                {this.state.autocompleteResults.hashtags.map((t,i)=>(
                                    <div key={t} style={{cursor:'pointer',backgroundColor:i===(this.state.idx - this.state.autocompleteResults.accounts.length)?AUTOCOMPLETE_SELECTED:'transparent'}} onClick={()=>this.showFullResults(`#${t}`)}>
                                        #{t}
                                    </div>
                                ))}
                            </div>
                        }
                    </div>
                }
            </div>
        );
    }
}

export default connector(SearchBox);