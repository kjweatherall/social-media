import { Post, Profile } from '../shared/SharedInterfaces';

export enum Page {
    HOME, // your wall
    ACCOUNT, // your account
    PROFILE, // user
    SEARCH, // search results
    FOLLOWERS // followers/following    
};

// payload for setPage action
export interface SetPagePayload {
    page: Page;
    handle?: string;
    skipRecordHistory?: boolean;
}

// action received by reducers
export interface ReduxAction {
    type: string;
    payload?: any;
}

// interface for account Redux state
export interface AccountState {
    account: Profile;
    viewing: SetPagePayload;
    profile: Profile;
    search: string;
};

// interface for posts Redux state
export interface PostsState {
    posts: Post[];
    profiles: Profile[];
    newPosts: Post[];
    hasMore: boolean;
}

// combined Redux state
export interface MainState {
    accountData:AccountState;
    postsData:PostsState;
}