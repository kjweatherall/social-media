/**
 * STYLES
 * Provides style data shared among components.
 */

export const COLOR_DARK = 'rgb(21, 32, 43)';
export const COLOR_LIGHT = 'rgb(56, 68, 77)';

export const BUTTON_COLOR = 'rgb(29, 161, 242)';

export const TEXT_COLOR = 'white';
export const TEXT_COLOR_SECONDARY = 'rgb(136, 153, 166)';
export const TEXT_COLOR_LINK = 'rgb(27, 149, 224)';
export const TEXT_COLOR_DELETE = 'rgb(224, 36, 94)';

export const COLOR_LIKED = 'green';
export const COLOR_UNLIKED = 'white';

export const SEARCH_BOX = 'rgb(37, 51, 65)';

export const AUTOCOMPLETE_SELECTED = 'green';

export const MENU_SELECTED = 'skyblue';

export const BACKGROUND_COLOR = COLOR_DARK;