// types
import { ReduxAction, PostsState } from '../../app-interfaces';
import { Post, SearchResults } from '../../../shared/SharedInterfaces';
import { ActionEnums } from '../actions/posts-actions';
import { FOLLOW_PROFILE, UNFOLLOW_PROFILE } from '../actions/account-actions';

const initialState: PostsState = {
    posts: [],
    profiles: [],
    newPosts: [],
    hasMore: false
};

export default (state = initialState, action: ReduxAction):PostsState =>{
    switch(action.type) {
        case ActionEnums.ADDED_POST:
            return {
                hasMore:state.hasMore,
                profiles:state.profiles,
                newPosts:state.newPosts,
                posts:[ action.payload ].concat(state.posts)
            }
        case ActionEnums.REMOVED_POST:
            let post = state.posts.find(p=>p.streamID === action.payload);

            if (!post) {
                return state;
            }

            let posts = state.posts.slice().filter(p=>p.streamID !== action.payload);

            for (let i = 0; i < posts.length; i++) {
                if (posts[i].postID === post.postID) {
                    posts[i] = Object.assign({}, posts[i], {repost:false});
                }
            }

            return {
                hasMore:state.hasMore,
                profiles:state.profiles,
                newPosts:state.newPosts,
                posts:posts
            };

        case ActionEnums.FETCHED_POSTS:
            if (action.payload.posts) {
                let updates: Partial<SearchResults> = { 
                    posts:action.payload.posts
                };
                if (action.payload.profiles) {
                    updates.profiles = action.payload.profiles;
                }

                return {
                    hasMore:action.payload.posts.hasMore,
                    newPosts:[],
                    posts:action.payload.posts.results,
                    profiles:action.payload.profiles
                };
            } else {
                return {
                    hasMore:action.payload.hasMore,
                    newPosts:[],
                    profiles:[],
                    posts:action.payload.results
                };
            }      
        case ActionEnums.OLDER_POSTS:
            return {
                hasMore:action.payload.hasMore,
                newPosts:state.newPosts,
                profiles:state.profiles,
                posts:state.posts.concat(action.payload.results)
            };

        case ActionEnums.LIKE_POST:{
            let posts = state.posts.slice();

            for (let i = 0; i < posts.length; i++) {
                if (posts[i].postID === action.payload) {
                    posts[i] = Object.assign({}, posts[i], {liked:1},{likes:posts[i].likes + 1});
                }
            }
            let obj = Object.assign({}, state, {posts});
            return obj;
        }
        case ActionEnums.UNLIKE_POST:{
            let posts = state.posts.slice();
            for (let i = 0; i < posts.length; i++) {
                if (posts[i].postID === action.payload) {
                    posts[i] = Object.assign({}, posts[i], {liked:0},{likes:posts[i].likes - 1});
                }
            }
            let obj = Object.assign({}, state, {posts});
            return obj;
        }
        case ActionEnums.NEW_POSTS:{
            return Object.assign({ }, state, {posts:(action.payload.results as Post[]).concat(state.posts)});
        }
        case FOLLOW_PROFILE:
        case UNFOLLOW_PROFILE:{
            let profiles = state.profiles.slice();
            for (let i = 0; i < profiles.length; i++) {
                if (profiles[i].handle === action.payload) {
                    profiles[i] = Object.assign({}, profiles[i], {connection:action.type === 'FOLLOW_PROFILE' ? 'following' : 'unconnected'});
                }
            }

            let posts = state.posts.slice();
            for (let i = 0; i < posts.length; i++) {
                if (posts[i].handle === action.payload) {
                    posts[i] = Object.assign({}, posts[i], {following:action.type === FOLLOW_PROFILE});
                }
            }

            return Object.assign({}, state, {profiles,posts});
        }
        case ActionEnums.REPOST_POST:
        case ActionEnums.UNREPOST_POST:{
            let posts = state.posts.slice();

            for (let i = 0; i < posts.length; i++) {
                if (posts[i].postID === action.payload) {
                    posts[i] = Object.assign({}, posts[i], {reposted:(action.type === ActionEnums.REPOST_POST ? true : false)});
                }
            }

            // return Object.assign({}, state, {posts});
            return {
                hasMore:state.hasMore,
                newPosts:state.newPosts,
                posts:posts,
                profiles:state.profiles
            };
        }
    }
    return state;
};