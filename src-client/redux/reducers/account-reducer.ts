// types
import { Page } from '../../app-interfaces';
import { analyseURL } from '../../helper';
import { SET_PAGE, SET_ACCOUNT, SET_BANNER, SET_PROFILE, SET_PROFILE_PHOTO, UPDATE_PROFILE, FOLLOW_PROFILE, UNFOLLOW_PROFILE } from '../actions/account-actions';
import { AccountState, ReduxAction, SetPagePayload } from '../../app-interfaces';

// analyse URL to get default state
let defaultPage = analyseURL();

const initialState: AccountState = {
    account: null,
    viewing:{
        page:defaultPage.page,
        handle:defaultPage.handle || null
    },
    profile:null,
    search:''
};

export default (state: AccountState = initialState, action: ReduxAction):AccountState=>{
    switch(action.type) {
        case SET_PAGE:
            let payload: SetPagePayload = action.payload;

            if (!payload.skipRecordHistory) {
                switch (payload.page) {
                    case Page.HOME:
                        window.history.pushState(null, 'Home', '/');
                        break;
                    case Page.PROFILE:
                        window.history.pushState(null, `Profile - ${payload.handle}`, `/user/${payload.handle}`);
                        break;
                    case Page.SEARCH:
                        window.history.pushState(null, `Search - ${payload.handle}`, `/search/${payload.handle.replace(/#/g, '%23')}`);
                        break;
                    case Page.FOLLOWERS:
                        window.history.pushState(null, `Profile - ${payload.handle}`, `/user/${payload.handle}/followers`);
                        break;
                }
            }

            return Object.assign({}, state, {viewing:payload,search:payload.page === Page.SEARCH ? payload.handle : ''});
        case SET_ACCOUNT:
            return Object.assign({}, state, {account:action.payload});        
        case SET_PROFILE:
            return Object.assign({}, state, {profile:action.payload});
        case SET_BANNER:{
            let changes = {
                bannerImage:action.payload.image + `?time=${new Date().getTime()}`,
                bannerImageThumbnail:action.payload.thumbnail + `?time=${new Date().getTime()}`                
            };

            return Object.assign({}, state, {
                account:Object.assign({}, state.account, changes),
                profile:Object.assign({}, state.profile, changes)
            });
        }
        case SET_PROFILE_PHOTO:{
            let changes = {
                profileImage:action.payload.image + `?time=${new Date().getTime()}`,
                profileImageThumbnailLarge:action.payload.thumbnailLarge + `?time=${new Date().getTime()}`,
                profileImageThumbnailSmall:action.payload.thumbnailSmall + `?time=${new Date().getTime()}`,
            };

            return Object.assign({}, state, {
                account:Object.assign({}, state.account, changes),
                profile:Object.assign({}, state.profile, changes)
            });
        }
        case UPDATE_PROFILE:
            return Object.assign({}, state, {
                account:Object.assign({}, state.account, action.payload),
                profile:Object.assign({}, state.profile, action.payload)
            });
        case FOLLOW_PROFILE:
            if (state.profile) {
                return Object.assign({}, state, {profile:Object.assign({}, state.profile, {connection:'following'})});
            }
            break;
        case UNFOLLOW_PROFILE:
            if (state.profile) {
                return Object.assign({}, state, {profile:Object.assign({}, state.profile, {connection:'unconnected'})});
            }
            break;
    }

    return state;
};