import { Post, SearchResults } from '../../../shared/SharedInterfaces';
import { ReduxAction } from '../../app-interfaces';

import { ThunkDispatch } from 'redux-thunk';
import { PostsState } from '../../app-interfaces';
import { AnyAction } from 'redux';

// type of Dispatch object
type Dispatch = ThunkDispatch<null, PostsState, AnyAction>;

// made post
const ADDED_POST = 'ADDED_POST';

function onAddedPost(post: Post): ReduxAction {
    return {
        type:ADDED_POST,
        payload:post
    }
}

let addPost = (text: string, handle: string, fileList?: FileList, callback?: (err?: string)=>void)=>{
    return (dispatch:Dispatch)=>{
        let formData = new FormData();

        formData.append('text', text);
        
        if (fileList) {
            formData.append('file', fileList[0], fileList[0].name);
        }

        let request = new XMLHttpRequest();
       
        request.addEventListener('load', (evt)=>{
            if (request.status === 200) {                
                let response = request.response;
                let post = JSON.parse(response);

                dispatch(onAddedPost(post));
                if (callback) {
                    callback();
                }
            } else {
                callback(request.responseText);
            }
        });

        request.open('POST', `/api/v1/users/${handle}/posts`);
        request.send(formData);
    };
};

// deleted post
const REMOVED_POST = 'REMOVED_POST';

function onRemovedPost(streamID: number): ReduxAction {
    return {
        type:REMOVED_POST,
        payload:streamID
    }
}

let deletePost = (streamID: number)=>{
    return (dispatch:Dispatch)=>{
        fetch(`/api/v1/posts/${streamID}`,{method:'delete'})
        .then(res=>{
            if (res.status === 204) {
                dispatch(onRemovedPost(streamID));
            }
        })
        .catch(err=>alert(`Error deleting post: ${err}`));
    };
};

// fetches posts
const FETCHED_POSTS = 'FETCHED_POSTS';

function onFetchedPosts(posts: Post[]): ReduxAction {
    return {
        type:FETCHED_POSTS,
        payload:posts
    }
}

let fetchPosts = (url: string)=>{
    return (dispatch: Dispatch)=>{
        fetch(url)
        .then(res=>res.json())
        .then(posts=>dispatch(onFetchedPosts(posts)))
        .catch(err=>console.error(`Error fetching posts: ${err}`));
    };
}

// fetches new posts
const NEW_POSTS = 'NEW_POSTS';

function onNewPosts(posts: Post[]): ReduxAction {
    return {
        type:NEW_POSTS,
        payload:posts
    }
}

let fetchNewPosts = (url:string)=>{
    return (dispatch:Dispatch)=>{
        fetch(url)
        .then(res=>{
            if (res.status === 200) {
                return res.json();
            }
        })
        .then(posts=>{
            if (posts) {
                dispatch(onNewPosts(posts));
            }
        })
        .catch(err=>alert(`Error fetching new posts: ${err}`));
    };
};

// fetches older posts
const OLDER_POSTS = 'OLDER_POSTS';

function onOlderPosts(posts: Post[]): ReduxAction {
    return {
        type:OLDER_POSTS,
        payload:posts
    }
}

let fetchOlderPosts = (url:string, callback?: ()=>void)=>{
    return (dispatch:Dispatch)=>{
        fetch(url)
        .then(res=>res.json())
        .then(posts=>{
            dispatch(onOlderPosts(posts));
            if (callback) {
                callback();
            }
        })
        .catch(err=>alert(`Error fetching older posts: ${err}`));
    };
};

// shows new posts
const SHOW_NEW_POSTS = 'SHOW_NEW_POSTS';

function onShowNewPosts() {
    return {
        type:SHOW_NEW_POSTS
    }
}

let showNewPosts = ()=>{
    return (dispatch:Dispatch)=>{
        dispatch(onShowNewPosts());
    };
};

// likes a post
const LIKE_POST = 'LIKE_POST';

function onLikePost(postID: number) {
    return {
        type:LIKE_POST,
        payload:postID
    }
}

let likePost = (postID: number)=>{
    return (dispatch:Dispatch)=>{
        fetch(`/api/v1/posts/${postID}/likes`, {method:'post'})
        .then(()=>dispatch(onLikePost(postID)))
        .catch(err=>alert(`Error liking post: ${err}`));
    };
}

// unlikes a post
const UNLIKE_POST = 'UNLIKE_POST';

function onUnlikePost(postID: number) {
    return {
        type:UNLIKE_POST,
        payload:postID
    }
}

let unlikePost = (postID: number)=>{
    return (dispatch:Dispatch)=>{
        fetch(`/api/v1/posts/${postID}/likes`, {method:'delete'})
        .then(()=>dispatch(onUnlikePost(postID)))
        .catch(err=>alert(`Error unliking post: ${err}`))
    };
};

// repost post
const REPOST_POST = 'REPOST_POST';

function onRepostPost(postID: number) {
    return {
        type:REPOST_POST,
        payload:postID
    }
}

let repostPost =(postID: number)=>{
    return (dispatch:Dispatch)=>{
        fetch(`/api/v1/posts/${postID}/repost`, {method:'post'})
        .then(res=>{
            if (res.status === 204) {
                dispatch(onRepostPost(postID));
            } else {
                res.text()
                .then(text=>alert(`Error reposting: ${text}`))
                .catch(err=>{
                    console.error(`Error reposting: ${err}`);
                    alert('Error reposting');
                });
            }
        })
        .catch(err=>{
            alert(`Error reposting: ${err}`);
        });
    };
}

// unrepost post
const UNREPOST_POST = 'UNREPOST_POST';

function onUnrepostPost(postID: number) {
    return {
        type:UNREPOST_POST,
        payload:postID
    }
}

let unrepostPost =(postID: number)=>{
    return (dispatch:Dispatch)=>{
        fetch(`/api/v1/posts/${postID}/repost`, {method:'delete'})
        .then(res=>{
            if (res.status === 204) {
                dispatch(onUnrepostPost(postID));
            } else {
                res.text()
                .then(text=>alert(`Error unreposting: ${text}`))
                .catch(err=>{
                    console.error(`Error unreposting: ${err}`);
                    alert('Error unreposting');
                });
            }
        })
        .catch(err=>{
            alert(`Error unreposting: ${err}`);
        });
    };
}

// exports 
export const ActionEnums = {
    ADDED_POST, REMOVED_POST, FETCHED_POSTS, NEW_POSTS, OLDER_POSTS, SHOW_NEW_POSTS, LIKE_POST, UNLIKE_POST, REPOST_POST, UNREPOST_POST
};

export const Actions = { 
    addPost, deletePost, fetchPosts, fetchNewPosts, fetchOlderPosts, showNewPosts, likePost, unlikePost, repostPost, unrepostPost
};