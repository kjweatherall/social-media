import { ReduxAction, SetPagePayload, Page } from '../../app-interfaces';
import { Profile } from 'interfaces';

// sets page being viewed
export const SET_PAGE = 'SET_PAGE';

let setPage = (page: Page, handle?: string, skipRecordHistory?: boolean)=>{
    return (dispatch:(action:ReduxAction)=>void)=>{
        let payload: SetPagePayload = {
            page,
            handle,
            skipRecordHistory
        };

        dispatch({
            type:SET_PAGE,
            payload:payload
        });
    };
};

// sets data of account of user logged in
export const SET_ACCOUNT = 'SET_ACCOUNT';

let setAccount = (account: Account)=>{
    return (dispatch:(action:ReduxAction)=>void)=>{
        dispatch({
            type:SET_ACCOUNT,
            payload:account
        });
    };
};

let fetchAccount = ()=>{
    return (dispatch:(action:ReduxAction)=>void)=>{
        fetch('/api/v1/users')
        .then(res=>res.json())
        .then(user=>dispatch({
            type:SET_ACCOUNT,
            payload:user
        }))
        .catch(err=>{
            alert(err);
        });
    };
};

// sets data for user currently being viewed
export const SET_PROFILE = 'SET_PROFILE';

function setProfile(handle: string) {
    return (dispatch:any)=>{
        fetch(`/api/v1/users/${handle}`)
        .then(res=>res.json())
        .then(profile=>dispatch({
            type:SET_PROFILE,
            payload:profile
        }))
        .catch();
    };
}

// sets banner for user
export const SET_BANNER = 'SET_BANNER';

function setBanner(handle: string, files: FileList) {
    return (dispatch:any)=>{
        let formData = new FormData();
        formData.append('file', files[0]);

        const opts = {
            method: 'POST',
            body:formData
        };

        fetch(`/api/v1/users/${handle}/banner`, opts)
        .then(res=>res.json())
        .then(data=>{
            if (data && data.image && data.thumbnail) {
                dispatch({
                    type:SET_BANNER,
                    payload:data
                });
            } else {
                alert(`Failed to update banner`);
            }
        })
        .catch(err=>alert(err));
    };
}

// sets profile photo for user
export const SET_PROFILE_PHOTO = 'SET_PROFILE_PHOTO';

function setProfilePhoto(handle:string, files: FileList) {
    return (dispatch:any)=>{
        let formData = new FormData();
        formData.append('file', files[0]);

        const opts = {
            method: 'POST',
            body:formData
        };

        fetch(`/api/v1/users/${handle}/photo`, opts)
        .then(res=>res.json())
        .then(data=>{
            if (data && data.image && data.thumbnailLarge && data.thumbnailSmall) {
                dispatch({
                    type:SET_PROFILE_PHOTO,
                    payload:data
                });
            } else {
                alert(`Failed to update profile photo`);
            }
        })
        .catch(err=>alert(err));
    };
}

// updates profile data for user
export const UPDATE_PROFILE = 'UPDATE_PROFILE';

function updateProfile(updates: Partial<Profile>) {
    return (dispatch:any)=>{
        dispatch({
            type:UPDATE_PROFILE,
            payload:updates
        });
    };
}

// follows a user
export const FOLLOW_PROFILE = 'FOLLOW_PROFILE';

function followProfile(handle: string, callback?:()=>void) {
    return (dispatch:any)=>{
        fetch(`/api/v1/users/${handle}/follow`, {method:'post'})
        .then(()=>{
            dispatch({
                type:FOLLOW_PROFILE,
                payload:handle
            });

            if (callback) {
                callback();
            }
        })
        .catch(alert);
    };
}

// unfollowers a user
export const UNFOLLOW_PROFILE = 'UNFOLLOW_PROFILE';

function unfollowProfile(handle: string, callback?: ()=>void) {
    return (dispatch:any)=>{
        fetch(`/api/v1/users/${handle}/follow`, {method:'delete'})
        .then(()=>{
            dispatch({
                type:UNFOLLOW_PROFILE,
                payload:handle
            });

            if (callback) {
                callback();
            }        
        })
        .catch(alert);
    };
}

export { setPage, setAccount, fetchAccount, setBanner, setProfilePhoto, setProfile, updateProfile, followProfile, unfollowProfile };