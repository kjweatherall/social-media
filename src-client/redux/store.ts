import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk, { ThunkDispatch } from 'redux-thunk';

import accountReducer from './reducers/account-reducer';
import postsReducer from './reducers/posts-reducer';

let reducers = combineReducers({
    accountData:accountReducer,
    postsData:postsReducer
});

import { fetchAccount } from './actions/account-actions';

const store = createStore(reducers, applyMiddleware<ThunkDispatch<{}, null, null>>(thunk));

// get user account as soon as possible
store.dispatch(fetchAccount());

export default store;