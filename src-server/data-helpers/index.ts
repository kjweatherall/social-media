import Accounts from './modules/Accounts';
import Followers from './modules/Followers';
import Posts from './modules/Posts';
import Profiles from './modules/Profiles';

export default {
    Accounts, Followers, Posts, Profiles
}