import DataHandler from '../../modules/DataHandler';

import { Profile } from '../../../shared/SharedInterfaces';
import Followers from './Followers';

export interface SQLProfile {
    display_name: string;
    handle: string;
    bio: string;
    profile_image: string;
    profile_image_thumbnail_small: string;
    profile_image_thumbnail_large: string;
    banner_image: string;
    banner_image_thumbnail: string;
    posts: number;
    followers: number;
    following: number;
}

async function get(accountID: number, requesterID?: number): Promise<Profile> {
    if (!requesterID) {
        requesterID = accountID;
    }

    let profile: SQLProfile = await DataHandler.queryDBGet(
        `SELECT display_name,handle,bio,profile_image,profile_image_thumbnail_small,profile_image_thumbnail_large,banner_image,banner_image_thumbnail,` + 
        `(SELECT count(*) FROM posts WHERE accountID=${accountID}) AS posts,` +
        `(SELECT count(*) FROM followers INNER JOIN accounts using(accountID) WHERE followingID=${accountID} AND (type!=0${requesterID?` OR accountID=${requesterID}`:''})) AS followers,` +
        `(SELECT count(*) FROM followers INNER JOIN accounts ON accounts.accountID=followingID WHERE followers.accountID=${accountID} AND (type!=0${requesterID?` OR followingID=${requesterID}`:''})) AS following ` +
        `FROM accounts ` + 
        `WHERE accountID=${accountID}`);

    let connection: 'self'|'following'|'unconnected';

    if (accountID === requesterID) {
        connection = 'self';
    } else {
        let isFollowing = await Followers.isFollowing(requesterID, accountID);
        connection = isFollowing ? 'following' : 'unconnected';
    }

    return {
        bannerImage:profile.banner_image,
        bannerImageThumbnail:profile.banner_image_thumbnail,
        bio:profile.bio,
        connection:connection,
        displayName:profile.display_name,
        followers:profile.followers,
        following:profile.following,
        handle:profile.handle,
        posts:profile.posts,
        profileImage:profile.profile_image,
        profileImageThumbnailSmall:profile.profile_image_thumbnail_small,
        profileImageThumbnailLarge:profile.profile_image_thumbnail_large
    };
}

export default { get };