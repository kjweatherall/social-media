/**
 * FOLLOWERS
 * Module for handling operations associated with followers.
 */

// types
import { Profile } from '../../../shared/SharedInterfaces';

// modules
import DataHandler from '../../modules/DataHandler';


// create required tables in database when it's ready
DataHandler.callWhenReady(()=>{
    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS followers(accountID NUMBER, followingID NUMBER, UNIQUE(accountID, followingID))')
    .catch(()=>process.exit(1));
});

/**
 * Adds a follower of an account.
 * @param accountID ID for account to follow another account
 * @param followingID ID for account to be followed
 * @returns If data was changed
 */
async function follow(accountID: number, followingID: number) {
    let results = await DataHandler.queryDBRun(`INSERT OR IGNORE INTO followers(accountID,followingID) VALUES(${accountID},${followingID})`);
    return (results.changes > 0);
}

/**
 * Removes a follower of an account.
 * @param accountID ID for account that is following another account
 * @param followingID ID for account that is being followed
 * @returns If data was changed
 */
async function unfollow(accountID: number, followingID: number) {
    let results = await DataHandler.queryDBRun(`DELETE FROM followers WHERE accountID=${accountID} AND followingID=${followingID}`);
    return (results.changes > 0);
}

/**
 * If an account is following another account.
 * @param accountID ID of account that is following
 * @param followingID ID of account being followed
 * @returns If account is following the other account
 */
async function isFollowing(accountID: number, followingID: number) {
    let results = await DataHandler.queryDBGet(`SELECT count(*) AS count FROM followers WHERE accountID=${accountID} AND followingID=${followingID}`);
    return (results.count != 0);
}

/**
 * If an account is being followed by another account.
 * @param accountID ID of account that is being followed
 * @param followerID ID of account that is following
 * @returns If the account is being followed by the other account
 */
async function isFollower(accountID: number, followerID: number) {
    let results = await DataHandler.queryDBGet(`SELECT count(*) AS count FROM followers WHERE accountID=${followerID} AND followingID=${accountID}`);
    return (results.count != 0);
}

/**
 * Gets data for each account being followed by account. A requester ID can be specified which will include if each account is following the requester account.
 * @param accountID ID of account to get followers for
 * @param requesterID ID of account requesting data
 * @param maxResults Max number of results returned
 */
async function getFollowing(accountID: number, requesterID?: number, maxResults?: number): Promise<Profile[]> {
    // generate statement
    let stmt = `SELECT followingID AS accountID,display_name,handle,bio,profile_image,profile_image_thumbnail_small,profile_image_thumbnail_large`;
    if (requesterID) {
        stmt += `,(SELECT count(*) FROM followers WHERE accountID=${requesterID} AND followingID=accounts.accountID) AS following `;
    }
    stmt += ` FROM followers INNER JOIN accounts ON followingID=accounts.accountID ` + 
        `WHERE followers.accountID=(SELECT accountID FROM accounts WHERE accountID=${accountID}) AND (type!=0${requesterID?` OR followingID=${requesterID}`:''})`;
    if (maxResults) {
        stmt += ` LIMIT ${maxResults}`;    
    }
    
    // execute statement
    let results = await DataHandler.queryDBAll(stmt);

    // parse accounts data and return results
    return results.map(r=>{
        let following = (r.following !== 0);
        delete r.following;
        r.connection = r.accountID === requesterID ? 'self' : following ? 'following' : 'unconnected';
        return r;
    })
}

/**
 * Gets data for each account follwing an account. A requester ID can be specified which will include if each account is following the requester account.
 * @param accountID ID of account to get followers for
 * @param requesterID ID of account requesting data
 * @param maxResults Max number of results returned
 */
async function getFollowers(accountID: number, requesterID?: number, maxResults?: number) {
    // generate statement
    let stmt = `SELECT accounts.accountID,display_name,handle,bio,profile_image,profile_image_thumbnail_small,profile_image_thumbnail_large`;
    if (requesterID) {
        stmt += `,(SELECT count(*) FROM followers WHERE accountID=${requesterID} AND followingID=accounts.accountID) AS following`;
    }
    stmt += ` FROM followers INNER JOIN accounts USING(accountID) ` +
        `WHERE followingID=${accountID} AND (type!=0${requesterID?` OR accountID=${requesterID}`:''})`;
    if (maxResults) {
        stmt += ` LIMIT ${maxResults}`;
    }
    
    // execute statement
    let results = await DataHandler.queryDBAll(stmt);

    // parse accounts data and return results
    return results.map(r=>{
        let following = (r.following !== 0);
        delete r.following;
        r.connection = r.accountID === requesterID ? 'self' : following ? 'following' : 'unconnected';
        return r;
    })
}

/**
 * Returns all accounts following and being followed by a specific account.
 * @param accountID ID of account to get data for
 * @param requesterID ID of account requesting data
 * @param maxResults Max results to be returned
 * @returns Accounts following and being followed
 */
async function getFollowersAndFollowing(accountID: number, requesterID?: number, maxResults?: number) {
    let results = await Promise.all([
        getFollowers(accountID, requesterID, maxResults),
        getFollowing(accountID, requesterID, maxResults)
    ]);

    return {
        followers:results[0],
        following:results[1]
    };
}

/**
 * Has account unfollow all accounts.
 * @param accountID ID of account
 * @returns If any changes have been made to data
 */
async function removeAllFollowing(accountID: number) {
    let results = await DataHandler.queryDBRun(`DELETE FROM followers WHERE accountID=${accountID}`);
    return (results.changes > 0);
}

/**
 * Has all accounts unfollow account.
 * @param accountID ID of account to unfollow
 * @returns If changes have been made to data
 */
async function removeAllFollowers(accountID: number) {
    let results = await DataHandler.queryDBRun(`DELETE FROM followers WHERE followingID=${accountID}`);
    return (results.changes > 0);
}

/**
 * Has account unfollow all accounts and all accounts unfollow it.
 * @param accountID ID for account
 * @returns If changes have been made to data
 */
function removeAllFollowersAndFollowing(accountID: number) {
    return Promise.all([
        removeAllFollowers(accountID),
        removeAllFollowing(accountID)
    ]);
}

export default {
    follow, unfollow, isFollowing, isFollower, getFollowing, getFollowers, removeAllFollowing, removeAllFollowers, removeAllFollowersAndFollowing, 
    getFollowersAndFollowing
}