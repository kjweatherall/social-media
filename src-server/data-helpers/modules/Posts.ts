/**
 * POSTS
 * Module for handling operations associated with posts.
 */

// types
import { Post, PostsQuery, PostsUpdate } from '../../../shared/SharedInterfaces';
import { Account } from '../../ServerInterfaces';

 // modules
import DataHandler from '../../modules/DataHandler';
import Accounts from './Accounts';

// create required tables in database when it's ready
DataHandler.callWhenReady(()=>{
    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS posts(postID INTEGER PRIMARY KEY, accountID NUMBER, published_at NUMBER, text TEXT COLLATE NOCASE)')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });

    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS posts_images(imageID INTEGER PRIMARY KEY, postID NUMBER, extension TEXT COLLATE NOCASE, thumbnailID INTEGER)')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });

    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS posts_likes(accountID INTEGER, postID INTEGER, published_at INTEGER, last_set INTEGER, status DEFAULT 1, UNIQUE(accountID, postID))')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });

    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS tags(tagID INTEGER PRIMARY KEY, tag STRING UNIQUE COLLATE NOCASE)')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });

    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS posts_tags(postID INTEGER, tagID INTEGER, UNIQUE(postID, tagID))')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });

    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS thumbnails(thumbnailID INTEGER PRIMARY KEY, extension STRING)')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });

    DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS accounts_posts(streamID INTEGER PRIMARY KEY, postID INTEGER, accountID INTEGER, repost INTEGER DEFAULT 0, added_at INTEGER, UNIQUE(postID, accountID, repost))')
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });
});

/**
 * Adds a new post.
 * @param accountID ID of account making post
 * @param text Text of post
 * @param imageFiles List of images included with post
 * @returns Post object representing new post
 */
async function create(accountID: number, text: string, imageFiles?: string|string[]) {
    // get current time as time posted
    const now = new Date().getTime();
    
    // replace any hashtag substitutions for hashtags
    text = text.replace(/%23/g, '#');

    // add post
    const results = await DataHandler.queryDBRun(`INSERT INTO posts(accountID,published_at,text) VALUES(${accountID},${now},?)`, [ text ]);
    let stream = await DataHandler.queryDBRun(`INSERT INTO accounts_posts(accountID,postID,added_at) VALUES(${accountID},${results.lastID},${now})`);

    // add any images
    if (imageFiles) {
        // if images specified as string, i.e. 1 image, turn into array so variable can be handle the same whether 1 or many images
        let imageArray: string[] = Array.isArray(imageFiles) ? imageFiles as string[] : [ imageFiles as string ];
        let ext = imageArray.map(i=>i.substring(i.lastIndexOf('.') + 1));
        await DataHandler.queryDBRun(`INSERT INTO posts_images(postID, extension) VALUES${ext.map(()=>`(${results.lastID},?)`).join(',')}`, ext);
    }

    // extract tags
    let prev = 0, idx;
    let tags:string[] = [ ];
    do {
        idx = text.indexOf('#', prev);

        if (idx !== -1) {
            let tag = '';

            let end = text.indexOf(' ', idx);
            if (end !== -1) {
                tag = text.substring(idx + 1, end);
                prev = end;
            } else {
                tag = text.substring(idx + 1);
                idx = -1;
            }

            if (tag && !tags.includes(tag)) {
                tags.push(tag.toLowerCase());
            }
        }
    } while (idx !== -1);

    // store any tags
    if (tags.length > 0) {
        await DataHandler.queryDBRun(`INSERT OR IGNORE INTO tags(tag) VALUES${tags.map(()=>'(?)')}`, tags);
        await DataHandler.queryDBRun(`INSERT OR IGNORE INTO posts_tags(postID,tagID) VALUES${tags.map(()=>`(${results.lastID},(SELECT tagID FROM tags WHERE tag=?))`)}`, tags);
    }

    // create and return data of new post
    const account = await Accounts.read({accountID:accountID}) as Account;

    // get IDs for images that were just added
    let images = await DataHandler.queryDBAll(`SELECT imageID FROM posts_images WHERE postID=${results.lastID}`);

    // create object representing new post
    let post: Post = {
        streamID: stream.lastID,
        postID: results.lastID,
        displayName: account.displayName,
        handle: account.handle,
        profileImage:account.profileImage,
        profileImageThumbnail:account.profileImageThumbnailSmall,
        text: text,
        publishedAt: now,
        likes:0,
        addedAt: now,
        following: false
    };

    // return Post object and IDs for images
    return {
        post,
        images:images.map(i=>+i.imageID)
    };
}

// structure of object returned when getting
export interface PostsResults {
    hasMore:boolean;
    results:Post[];
}

// structure of object returned when querying database
interface SQLPost {
    streamID: number;
    postID: number;
    accountID: number;
    text: string;
    images: string;
    thumbnails: string;
    display_name: string;
    handle: string;
    likes: number;
    profile_image: string;
    profile_image_thumbnail: string;
    liked: number;
    published_at: number;
    poster: number;
    repost: number;
    reposted: number;
    reposted_by_name: string;
    reposted_by_handle: string;
    added_at: number;
    following: number;
}

/**
 * Gets posts based on specified parameters.
 * @param query Parameters to get posts for
 * @param accountID ID of account requesting data
 * @returns Applicable post(s)
 */
async function read(query: PostsQuery, accountID?: number): Promise<PostsResults|Post|Post[]> {
    let params: string[] = [ ];    
    
    // generate statement
    let stmt = 
        'SELECT streamID,posts.*,accounts_posts.accountID AS poster,accounts_posts.added_at,accounts_posts.repost,' +
        'GROUP_CONCAT(posts_images.imageID || "." || posts_images.extension) AS images,' +
        'GROUP_CONCAT(posts_images.thumbnailID || ".jpg") AS thumbnails,' +
        'accounts.display_name,accounts.handle,' +
        'accounts.profile_image,accounts.profile_image_thumbnail_small AS profile_image_thumbnail,' +        
        'reposter.display_name AS reposted_by_name,reposter.handle AS reposted_by_handle';
    if (accountID) {
        stmt += `,(SELECT count(*) FROM posts_likes WHERE posts_likes.accountID=${accountID} AND posts_likes.postID=posts.postID AND status=1) AS liked,` +
            `(SELECT count(*) FROM posts_likes INNER JOIN posts USING(postID) INNER JOIN accounts USING(accountID) WHERE postID=accounts_posts.postID AND status=1 AND (accounts.type != 0 OR posts_likes.accountID=${accountID})) AS likes,` +
            `(SELECT count(*) FROM followers WHERE accountID=${accountID} AND followingID=posts.accountID) AS following,` +
            `(SELECT count(*) FROM accounts_posts AS ap INNER JOIN posts USING(postID) WHERE ap.postID=accounts_posts.postID AND ap.accountID=${accountID} AND repost=1) AS reposted`;
    } else {
        stmt += ',(SELECT count(*) FROM posts_likes INNER JOIN posts USING(postID) INNER JOIN accounts USING(accountID) WHERE postID=posts.postID AND status=1 AND type!=0) AS likes';
    }

    stmt += ' FROM accounts_posts INNER JOIN posts USING(postID) INNER JOIN accounts ON accounts.accountID=posts.accountID LEFT JOIN posts_images USING(postID) LEFT JOIN accounts AS reposter ON accounts_posts.accountID=reposter.accountID';

    // if specific posts were specified, get and return those
    if (query.postIDs) {
        if (typeof query.postIDs === 'number') {
            return await DataHandler.queryDBGet(`${stmt} WHERE postID=${query.postIDs}`);
        } else {
            return await DataHandler.queryDBAll(`${stmt} WHERE postID IN (${query.postIDs.join(',')})`);
        }
    } else if (query.imageID) {
        return await DataHandler.queryDBGet(`${stmt} WHERE imageID=${query.imageID}`);
    }

    // conditions to get posts for
    let conditions: string[] = [ 
        accountID ? `(accounts.type != 0 OR posts.accountID=${accountID})` : `accounts.type != 0`
    ];

    // account ID or handle related to posts
    if (query.accountID || query.handle) {
        // turn handle into account ID if specified, otherwise use account ID
        if (query.handle) {
            var aID: number = (await DataHandler.queryDBGet('SELECT accountID FROM accounts WHERE handle=?', query.handle)).accountID;
        } else {
            var aID = query.accountID;
        }

        // "own" specifies this is for the account ID's stream, so posts should include their own content and the content of those they follow
        if (query.own) {
            conditions.push(`(accounts_posts.accountID=${aID} OR accounts_posts.accountID IN (SELECT followingID FROM followers WHERE accountID=${aID}))`);
        } else {
            conditions.push(`accounts_posts.accountID=${aID}`);
        }
    }
    
    // published at
    if (query.publishedAt) {
        switch (query.publishedAt.relativeToTime) {
            case 'before':
                var symbol = '<';
                break;
            case 'after':
                var symbol = '>';
                break;
            case 'equals':
                var symbol = '='
                break;
        }
        if (query.publishedAt.includeTime && query.publishedAt.relativeToTime !== 'equals') {
            symbol += '=';
        }
        conditions.push(`accounts_posts.added_at${symbol}${query.publishedAt.time}`);
    }

    // extension
    if (query.ext) {
        conditions.push('extension=?');
        params.push(query.ext);
    }

    // exclude reposts
    // TODO: Add support for this
    if (query.excludeReposts) {
        conditions.push('posts.accountID=poster');
    }

    // text to search for within post text
    if (query.text) {
        if (query.text.exact) {
            conditions.push('text=?');
            params.push(query.text.value);
        } else {
            let text: string[] = [ ],
                tags: string[] = [ ];
            
            query.text.value.split(' ').forEach(t=>{
                if (t !== '#') {
                    if (t[0] === '#') {
                        tags.push(t.substring(1));
                    } else {
                        text.push(t);
                    }
                }
            });

            if (text.length > 0) {
                conditions.push(text.map(()=>`text LIKE ?`).join(' AND '));
                params = params.concat(text.map(t=>`%${t}%`));
            } 
            
            if (tags.length > 0) {
                conditions.push(tags.map(()=>`? IN (SELECT tag FROM posts_tags INNER JOIN tags USING(tagID) WHERE postID=posts.postID)`).join(' AND '));
                params = params.concat(tags);
            }
        }
    }

    // append conditions to statement if any specified
    if (conditions.length > 0) {
        stmt += ` WHERE ${conditions.join(' AND ')}`;
    }
    
    stmt += ` GROUP BY accounts_posts.rowid ORDER BY added_at DESC`;

    // max results
    if (query.maxResults) {
        stmt += ` LIMIT ${query.maxResults + 1}`;
    }

    // execute statement
    let results: SQLPost[] = await DataHandler.queryDBAll(stmt, params);

    // determine if there are more posts than just those requested
    let hasMore = false;

    if (query.maxResults && results.length === query.maxResults + 1) {
        hasMore = true;
        results.splice(results.length - 1, 1);
    }

    // parse posts and return results
    return {        
        results:results.map(p=>({
            streamID:p.streamID,
            displayName:p.display_name,
            handle:p.handle,
            liked:p.liked != 0,
            likes:p.likes,
            postID:p.postID,
            profileImage:p.profile_image,
            profileImageThumbnail:p.profile_image_thumbnail,
            publishedAt:p.published_at,
            text:p.text,
            images:p.images,
            thumbnails:p.thumbnails,
            repost:(p.repost > 0),
            reposted:(p.reposted > 0),
            repostedByName:(p.poster === accountID ? 'you' : p.reposted_by_name),
            repostedByHandle:p.reposted_by_handle,
            addedAt:p.added_at,
            following:(p.following > 0)
        } as Post)),
        hasMore:hasMore
    };
}

/**
 * Updates a post.
 * @param data Data to update post with
 * @returns 
 */
function update(data: PostsUpdate) {
    if (!data.postID) {
        throw 'No post ID specified for post update';
    }

    let stmt = 'UPDATE posts SET ';
    let changes: string[] = [ ];
    let params: string[] = [ ];

    if (data.publishedAt) {
        changes.push(`published_at=${data.publishedAt}`);
    }
    if (data.text) {
        changes.push(`text=?`);
        params.push(data.text);
    }

    stmt += ` ${changes.join(',')} WHERE postID=${data.postID}`;
    return DataHandler.queryDBRun(stmt, params);
}

async function remove(streamID: number) {
    // get post
    let stream = await DataHandler.queryDBGet(`SELECT postID,repost FROM accounts_posts WHERE streamID=${streamID}`);

    // no post with stream ID
    if (!stream) {
        throw 'Stream ID not found';
    }

    // post is repost; just remove repost
    if (stream.repost) {
        let results = await DataHandler.queryDBRun(`DELETE FROM accounts_posts WHERE streamID=${streamID}`);
        return (results.changes > 0); 
    }

    // post was not repost; delete original
    let postID = stream.postID;

    let results = await DataHandler.queryDBRun(`DELETE FROM posts WHERE postID=${postID}`);    
    await DataHandler.queryDBRun(`DELETE FROM thumbnails WHERE thumbnailID IN (SELECT thumbnailID FROM posts_images WHERE postID=${postID})`);
    await DataHandler.queryDBRun(`DELETE FROM posts_images WHERE postID=${postID}`);
    await DataHandler.queryDBRun(`DELETE FROM accounts_posts WHERE postID=${postID}`);
    await DataHandler.queryDBRun(`DELETE FROM posts_likes WHERE postID=${postID}`);
    await DataHandler.queryDBRun(`DELETE FROM posts_tags WHERE postID=${postID}`);
    return (results.changes > 0);
}

async function removeImage(imageID: number) {
    let results = await DataHandler.queryDBRun(`DELETE FROM posts_images WHERE imageID=${imageID}`);
    return (results.changes > 0);
}

async function like(accountID: number, postID: number) {
    let now = new Date().getTime();
    let results = await DataHandler.queryDBRun(`INSERT INTO posts_likes(accountID,postID,published_at,last_set) VALUES(${accountID},${postID},${now},${now}) ON CONFLICT(accountID,postID) DO UPDATE SET status=1`);
    return (results.changes > 0);
}

async function unlike(accountID: number, postID: number) {
    let now = new Date().getTime();
    let results = await DataHandler.queryDBRun(`INSERT INTO posts_likes(accountID,postID,published_at,last_set,status) VALUES(${accountID},${postID},${now},${now},0) ON CONFLICT(accountID,postID) DO UPDATE SET status=0`);
    return (results.changes > 0);
}

async function getLikes(postID: number, accountID?: number) {
    let stmt = `SELECT count(*) FROM posts_likes INNER JOIN posts USING(postID) INNER JOIN(accountID) WHERE postID=${postID} AND status=1 AND `;
    if (accountID) {
        stmt += `(type != 0 OR accountID=${accountID})`;
    } else {
        stmt += 'type != 0';
    }
    let results = await DataHandler.queryDBGet(stmt);
    return results["count(*)"];
}

function repost(accountID: number, postID: number) {
    return DataHandler.queryDBRun(`INSERT OR IGNORE INTO accounts_posts(accountID,postID,added_at,repost) VALUES(${accountID},${postID},${new Date().getTime()},1)`);
}

function unrepost(accountID: number, postID: number) {
    return DataHandler.queryDBRun(`DELETE FROM accounts_posts WHERE accountID=${accountID} AND postID=${postID} AND repost=1`);
}

export default {
    create, read, update, remove, removeImage, like, unlike, getLikes, repost, unrepost
}