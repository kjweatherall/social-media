/**
 * ACCOUNTS
 * Module for handling operations associated with accounts.
 */

// types
import { Account, AccountsQuery, AccountsUpdate } from '../../ServerInterfaces';

// modules
import DataHandler from '../../modules/DataHandler';
import Crypto from '../../modules/Crypto';

// create required tables in database when it's ready
DataHandler.callWhenReady(()=>{
    DataHandler.queryDBRun(
        'CREATE TABLE IF NOT EXISTS accounts(accountID INTEGER PRIMARY KEY, display_name TEXT NOT NULL COLLATE NOCASE, handle TEXT NOT NULL UNIQUE COLLATE NOCASE,' + 
        'username TEXT NOT NULL UNIQUE COLLATE NOCASE, password TEXT NOT NULL, salt TEXT NOT NULL, profile_image TEXT, banner_image TEXT, profile_image_thumbnail_small TEXT,' +
        'profile_image_thumbnail_large TEXT, banner_image_thumbnail TEXT,bio TEXT COLLATE NOCASE, type INTEGER DEFAULT 0, created_at INTEGER, api_key TEXT UNIQUE)')
    .then(()=>DataHandler.queryDBRun('CREATE TABLE IF NOT EXISTS accounts_sessions(accountID NUMBER UNIQUE, session_code TEXT)'))
    .catch(err=>{
        console.error(err);
        process.exit(1);
    });
});

// structure of data returned from Accounts query
export interface SQLAccount {
    accountID: number;
    display_name: string;
    handle: string;
    username: string;
    password: string;
    salt: string;
    profile_image: string;
    profile_image_thumbnail_small: string;
    profile_image_thumbnail_large: string;
    banner_image: string;
    banner_image_thumbnail: string;
    bio: string;
    type: number;
    created_at: number;
    session_code?: string;
}

// converts data returned from Accounts query to JS object
// TODO: Determine if this is really necessary. SQL seems to use underscore to separate words in a variable name, but JavaScript uses camel case. the purpose
// of this function is to convert from SQL to JS, but is there a better way? Should the database query just automatically return each variable in JS style?
function sqlAccountToAccount(account: SQLAccount) {
    if (!account) {
        return null;
    }

    return {
        accountID:account.accountID,
        bannerImage:account.banner_image,
        bannerImageThumbnail:account.banner_image_thumbnail,
        bio:account.bio,
        displayName:account.display_name,
        handle:account.handle,
        password:account.password,
        profileImage:account.profile_image,
        profileImageThumbnailLarge:account.profile_image_thumbnail_large,
        profileImageThumbnailSmall:account.profile_image_thumbnail_small,
        salt:account.salt,
        sessionCode:account.session_code,
        type:account.type
    } as Account;
}

/**
 * Adds a new account to database.
 * @param displayName Display name for account
 * @param handle Handle for account
 * @param username Username for account
 * @param password Plain-text password for account
 * @returns ID for new account
 */
async function create(displayName: string, handle: string, username: string, password: string, type = 1): Promise<number> {
    // ensure required parameters are specified
    if (!displayName) {
        throw 'No display name specified';
    }
    if (!handle) {
        throw 'No handle specified';
    }
    if (!username) {
        throw 'No username specified'
    }

    // hash password    
    let passwordData = Crypto.hashPassword(password);

    // add data to database
    let now = new Date().getTime();
    try {
        var results = await DataHandler.queryDBRun(`INSERT INTO accounts(display_name, handle, username, password, salt, created_at, type) VALUES(?,?,?,?,?,${now},${type})`, [ displayName, handle, username, passwordData.passwordHash, passwordData.salt ]);
    } catch(err) {        
        throw err;
    }

    return results.lastID;
}

async function createGuest(): Promise<number> {
    let lastAccount = await DataHandler.queryDBGet('SELECT accountID FROM accounts ORDER BY accountID DESC');

    if (lastAccount) {
        var accountID: number = lastAccount.accountID + 1;
    } else {
        var accountID = 1;
    }

    const account = await create(`Guest`, `guest_${accountID}`, `guest-${accountID}-${((Math.random() * 10000) >>> 0)}`, '', 0);

    // automatically follow admin(s)
    await DataHandler.queryDBRun(`INSERT OR IGNORE INTO followers(accountID,followingID) SELECT ${account},accountID FROM accounts WHERE type=2`);

    return account;
}

/**
 * Gets account(s) based on specified parameters.
 * @param query Parameters for accounts to return
 * @returns Account(s) that match parameters
 */
async function read(query: AccountsQuery): Promise<Account|Account[]> {
    let stmt = `SELECT accounts.*, session_code FROM accounts LEFT JOIN accounts_sessions USING(accountID) WHERE `;
    let conditions: string[] = [ ];
    let params: string[] = [ ];

    // account ID
    if (query.accountID) {
        conditions.push(`accountID=${query.accountID}`);
    }

    // display name
    if (query.displayName) {
        if (query.displayName.exact) {
            conditions.push('display_name=?');
            params.push(query.displayName.value);
        } else {
            conditions.push('display_name LIKE ?');
            params.push(`%${query.displayName.value}%`);
        }            
    }

    // username
    if (query.username) {
        if (query.username.exact) {
            conditions.push('username=?');
            params.push(query.username.value);
        } else {
            conditions.push('username LIKE ?');
            params.push(`%${query.username.value}%`);
        }            
    }

    // thumbnail
    if (query.thumbnail) {
        conditions.push(`thumbnail=${query.thumbnail}`);
    }

    // search
    if (query.search) {
        conditions.push('display_name LIKE ?');
        params.push(`%${query.search}%`);
    }

    // add conditions to statement
    stmt += conditions.join(' AND ');

    // execute statement and return results
    if (query.accountID) {
        return sqlAccountToAccount(await DataHandler.queryDBGet(stmt, params));
    } else if (conditions.length > 0) {
        if (query.maxResults) {
            stmt += ` LIMIT ${query.maxResults}`;
        }
        return (await DataHandler.queryDBAll(stmt, params)).map(acc=>sqlAccountToAccount(acc));
    } else {
        throw 'No conditions specified for account query';
    }
}

/**
 * Updates account data.
 * @param accountID ID for account to update
 * @param data Data to change for account
 */
function update(accountID: number, data: AccountsUpdate) {
    let stmt = `UPDATE accounts SET `;
    let changes: string[] = [ ];
    let params: string[] = [ ];
    
    // display name
    if (data.displayName) {
        changes.push('display_name=?');
        params.push(data.displayName);
    }

    // handle
    if (data.handle) {
        changes.push('handle=?');
        params.push(data.handle);
    }

    // bio
    if (typeof data.bio !== 'undefined') {
        changes.push('bio=?');
        params.push(data.bio);
    }

    // username
    if (data.username) {
        changes.push('username=?');
        params.push(data.username);
    }

    // password
    if (data.password) {
        let passwordData = Crypto.hashPassword(data.password);
        changes.push('password=?,salt=?');
        params.push(passwordData.passwordHash);
        params.push(passwordData.salt);
    }

    // execute statement and return results
    if (changes.length > 0) {
        stmt += `${changes.join(',')} WHERE accountID=${accountID}`;
        return DataHandler.queryDBRun(stmt, params);
    } else {
        throw 'No data provided to update account';
    }
}

/**
 * Deletes account data from database.
 * @param accountID ID for account to delete
 * @returns If any rows were deleted
 */
async function remove(accountID: number) {
    let results = await Promise.all([
        DataHandler.queryDBRun(`DELETE FROM accounts WHERE accountID=${accountID}`),
        clearSessionCode(accountID)
    ]);
    return (results[0].changes > 0);
}

/**
 * Checks if session code matches that of specified account.
 * @param accountID ID of account to check
 * @param sessionCode Session code to check
 * @returns If the session code is that for the account
 */
async function isSessionValid(accountID: number, sessionCode: string) {
    let account = await read({accountID:accountID}) as Account;
    return (account && account.sessionCode === sessionCode);
}

/**
 * Returns ID of account that matches specified username and password.
 * @param username Username to check
 * @param password Plain-text password to check
 */
async function login(username: string, password: string) {
    // username required; check if specified
    if (!username) {
        throw 'No username specified';
    }

    let account = await read({username:{value:username,exact:true}}) as Account[];

    // if no account with username, no match
    if (account.length === 0 || account[0].type === 0) {
        return 0;
    }

    // hash specified password to compare to stored password
    let passwordData = Crypto.saltPassword(password, account[0].salt);

    // return account ID if password matches
    if (passwordData.passwordHash !== account[0].password) {
        return 0;
    } else {
        return account[0].accountID;
    }
}

// length of session code
const SESSION_LEN = 32;
// valid characters for session code
const SESSION_CHARS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890';

/**
 * Generates and stores a new session code for the specified account.
 * @param accountID ID of account to generate session code for
 * @returns The new session code
 */
async function generateSessionCode(accountID: number) {
    // check account exists
    let account = await read({accountID}) as Account;
    if (!account) {
        throw 'Account to generate session code for cannot be found.';
    }

    // generate code
    let sessionCode = '';
    for (let i = 0; i < SESSION_LEN; i++) {
        sessionCode += SESSION_CHARS[((Math.random() * SESSION_CHARS.length) >>> 0)];
    }
    
    // store code and return it
    let results = await DataHandler.queryDBRun(`INSERT INTO accounts_sessions(accountID,session_code) VALUES(${accountID},?) ON CONFLICT(accountID) DO UPDATE SET session_code=?`, [ sessionCode, sessionCode ]);
    if (results.changes > 0) {
        return sessionCode;
    } else {
        throw 'Error setting session code';
    }
}

/**
 * Removes the session code for an account.
 * @param accountID ID for account to remove session code for
 */
function clearSessionCode(accountID: number) {
    return DataHandler.queryDBRun(`DELETE FROM accounts_sessions WHERE accountID=${accountID}`);
}

export default {
    create, read, update, remove, login, isSessionValid, generateSessionCode, clearSessionCode, createGuest
}