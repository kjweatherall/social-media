////////////////
///// MISC /////
////////////////

type QueryMatch = {
    value: string;
    exact?: boolean;
};

export interface Query {
    time?: number;
    timespan?: 'before'|'after';
    amount?: number;
    accountID?: number;
    ids?: number[];
}

export interface Settings {
    port: number;
    dbPath: string;
    cookieSecret: string;
    debug?: boolean;
}

////////////////////
///// ACCOUNTS /////
////////////////////

export interface Account {
    accountID: number;
    displayName: string;    
    handle: string;    
    password: string;
    salt: string;
    sessionCode: string;
    profileImage: string;
    profileImageThumbnailSmall: string;
    profileImageThumbnailLarge: string;
    bannerImage: string;
    bannerImageThumbnail: string;
    bio: string;
    type: number;
}

export interface AccountsQuery {
    accountID?: number;
    displayName?: QueryMatch;
    handle?: QueryMatch;
    username?: QueryMatch;
    thumbnail?: number;
    search?: string;
    maxResults?: number;
}

export interface AccountsUpdate {
    displayName?: string;
    handle?: string;
    username?: string;
    password?: string;
    bio?: string;
}

///////////////////////
///// CREDENTIALS /////
///////////////////////

export interface Credentials {
    accountID: number;
}