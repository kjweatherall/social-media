let timer: NodeJS.Timeout = null;

interface Task {
    callback(): void;
    frequency: number;
}

interface TaskData {
    task: Task;
    lastExecute: number;
    id: number;
}

let taskID = 1;
let tasks: TaskData[] = [ ];

function start() {
    if (timer) {
        return;
    }

    timer = setInterval(update, 1000 * 60);
}

function stop() {
    if (!timer) { 
        return;
    }

    clearInterval(timer);
    timer = null;
}

function update() {
    let now = new Date().getTime();

    for (let t of tasks) {
        if ((now - t.lastExecute) / 60000 >= t.task.frequency) {
            t.task.callback();
            t.lastExecute = now;
        }
    }
}

function addTask(callback: ()=>void, frequency: number) {
    let task: TaskData = {
        task:{
            callback,
            frequency
        },
        lastExecute:new Date().getTime(),
        id:taskID++
    };

    tasks.push(task);

    return task.id;
}

function removeTask(id: number) {
    let len = tasks.length;
    if (len === 0) {
        console.warn('Tried to remove task when task list is empty');
        return false;
    }

    let idx = tasks.findIndex(t=>t.id === id);
    if (idx !== -1) {
        tasks.splice(idx, 1);
        return true;
    } else {
        console.warn(`Tried to remove task that was not in list`);
        return false;
    }
}

function isRunning() {
    return timer !== null;
}

start();

export default {
    start, stop, addTask, removeTask, isRunning
}