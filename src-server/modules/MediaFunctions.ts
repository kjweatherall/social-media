/**
 * MEDIA FUNCTIONS
 * Provides functions for dealing with images.
 */

import * as fs from 'fs';
import {spawn} from 'child_process';

interface ImageData {
    type:string;
    dimensions:{
        width:number;
        height:number;
    };
    bits:string;
    color:string;
}

const Functions = {
    getImageData(filepath: string): Promise<ImageData> {
        return new Promise((resolve, reject)=>{
            if (process.platform === 'win32') {
                var image = spawn('magick', ['identify',filepath]);
            } else {
                var image = spawn('convert', ['-identify',filepath,filepath]);
            }
    
            let output = '';
    
            image.stdout.on('data', chunk=>output += chunk);
    
            image.on('exit', (code, signal)=>{
                if (!code) {
                    let split = output.split(' ');
    
                    let dimensions = null;
    
                    if (split[2]) {
                        let d = split[2].split('x');
    
                        dimensions = {
                            width:+d[0],
                            height:+d[1]
                        };
                    }
    
                    resolve({
                        type:split[1],
                        dimensions:dimensions,
                        bits:split[4],
                        color:split[5]
                    });
                } else {
                    reject();
                }
            });
    
            image.on('error', err=>{
                console.error(`Error getting image data: ${err}`);
                reject();
            })
        });    
    },
    /**
     * Produces a resized version of an image.
     * @param oldPath Path of image to resize
     * @param newPath Path for new image
     * @param dimensions Dimensions for new image
     */
    convertImage(oldPath: string, newPath: string, dimensions?: {width:number, height:number}) {
        return new Promise((resolve, reject)=>{
            let params: string[] = [ oldPath ];
            if (dimensions) {
                params.push('-resize');
                params.push(`${dimensions.width}x${dimensions.height}`);
            }
            params.push(newPath);

            if (process.platform === 'win32') {
                var convert = spawn('magick', params);
            } else {
                var convert = spawn('convert', params);
            }

            let outputText = '';
            convert.stderr.on('data', data=>outputText += data);
            convert.on('exit', (code)=>{
                if (!code) {
                    resolve();
                } else {
                    console.error(`Error converting image:`);
                    console.error(outputText);
                    reject();
                }
            });
        });
    },
    determineExtension(filePath: string): Promise<string> {
        return new Promise(function(resolve, reject) {
            if (!fs.existsSync(filePath)) {
                console.error(`File to determine extension for "${filePath}" does not exist`);
                reject();
            } else {
                let output = "";
    
                let media = spawn('ffmpeg', [ '-i', filePath, '-hide_banner']);
                media.stderr.on('data', data=>{
                    output += data;
                });
                media.on('exit', (code: number, signal: string)=>{
                    let idx = output.indexOf('Input #0,');
                    if (idx !== -1) {
                        let end = output.indexOf(', from ', idx + 9);
                        if (end !== -1) {
                            let val = output.substring(idx + 9, end).trim();
                            switch(val) {
                                case "image2":
                                case "jpeg_pipe":
                                    return resolve('jpg');
                                case "gif":
                                    return resolve('gif');                                    
                                case "png_pipe":
                                    return resolve('png');
                                case "bmp_pipe":
                                    return resolve('bmp');
                                case "ogg":
                                    if (output.includes('Video:')) {
                                        if (!output.includes('Video: none')) {
                                            resolve('ogv');
                                        } else {
                                            console.error(`File "${filePath}" is possibly an audio file.`);
                                            reject();
                                        }
                                    } else {
                                        console.error(`"${filePath}" is OGG, but can't determine if video or audio.`);
                                        reject();
                                    }
                                    return;
                                case "rm":
                                    return resolve('rmvb');
                                case "mpeg":
                                    return resolve('mpeg');                                    
                                case "mp3":
                                    return resolve('mp3');
                            }
    
                            // TODO: Add support for 3gp, and any others
    
                            if (val.includes('mp4')) {
                                return resolve('mp4');
                            } else if (val.includes('matroska')) {
                                return resolve('mkv');
                            } else if (val.includes('webm')) {
                                return resolve('webm');
                            } else if (val.includes('avi')) {
                                return resolve('avi');
                            } else if (val.includes('flv')) {
                                return resolve('flv');
                            }
                        }
                    }
    
                    console.warn(`Cannot determine extension for ${filePath}`);
                    console.warn(output);
                    reject();
                });
            }
        })
    },
    makeImageThumbnail(input: string, output: string, width: number, height: number): Promise<void> {
        return new Promise(async function(resolve, reject) {
            try {
                var data = await Functions.getImageData(input);
            } catch(err) {
                console.error(err);
                var data: ImageData = null;
            }
    
            if (data && data.type === 'GIF') {
                input += '[0]';
            }
        
            let size = `${width}x${height}`;

            let params = [ input, '-resize', `${size}^`, '-gravity', 'center', '-extent', size, output ];

            if (process.platform === 'win32') {
                var convert = spawn('magick', params);
            } else {
                var convert = spawn('convert', params);
            }

            let outputText = '';
    
            convert.stderr.on('data', chunk=>{
                outputText += chunk;
            });
    
            convert.on('exit', (code, signal)=>{
                if (!code) {
                    resolve();
                } else {
                    console.error(outputText);
                    reject();
                }
            });
        });
    }
};

export default Functions;