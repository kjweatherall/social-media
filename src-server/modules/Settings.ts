/**
 * SETTINGS
 * Loads settings from a while and provides access to those settings.
 */

import * as fs from 'fs';

class Settings {
    settings: {[s:string]:any};

    load(filename: string) {
        // load settings    
        if (fs.existsSync(filename)) {
            let data = fs.readFileSync(filename, 'utf8');
            this.settings = JSON.parse(data);
            return true;
        } else {
            console.warn(`No settings file at ${filename}!`);
            return false;
        }
    }
}

let settings = new Settings();

export default settings;