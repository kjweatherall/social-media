import * as fs from 'fs';
import * as path from 'path';

// settings - required by DataHandler
import Settings from './Settings';
Settings.settings = { dbPath:'test.db' };

// modules
import DataHandler from './DataHandler';

beforeAll(done=>{
    DataHandler.callWhenReady(async ()=>{
        await DataHandler.queryDBRun(`CREATE TABLE test(id INTEGER PRIMARY KEY, name STRING NOT NULL, username STRING NOT NULL UNIQUE)`);
        done();
    });
});

afterAll(async done=>{
    await DataHandler.close();
    fs.unlinkSync(path.join(__dirname, Settings.settings.dbPath));
    done();
});

test('Inserting into database', ()=>{    
    return expect(DataHandler.queryDBRun(`INSERT INTO test(name,username) VALUES("test", "username"),("test2","username2")`)).resolves.toEqual({lastID:2,changes:2});
});

test('Get row after insert', ()=>{
    return expect(DataHandler.queryDBGet(`SELECT * FROM test`)).resolves.toEqual({id:1,name:'test',username:'username'});
});

test('Updating 1 row in database', ()=>{
    return expect(DataHandler.queryDBRun('UPDATE test SET name="changed",username="username-changed" WHERE id=1')).resolves.toEqual({lastID:2,changes:1});
});

test('Get row after update 1 row', ()=>{
    return expect(DataHandler.queryDBGet(`SELECT * FROM test`)).resolves.toEqual({id:1,name:'changed',username:'username-changed'});
});

test('Updating all rows in database', ()=>{
    return expect(DataHandler.queryDBRun('UPDATE test SET name="identical"')).resolves.toEqual({lastID:2,changes:2});
});

test('Get row after update 1 row', ()=>{
    const EXPECTED_RESULT = [
        {id:1,name:'identical',username:'username-changed'},
        {id:2,name:'identical',username:'username2'}
    ];
    return expect(DataHandler.queryDBAll(`SELECT * FROM test`)).resolves.toEqual(EXPECTED_RESULT);
});

test('Delete all rows database', ()=>{
    return expect(DataHandler.queryDBRun(`DELETE FROM test`)).resolves.toEqual({lastID:2,changes:2});
});