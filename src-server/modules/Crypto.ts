/**
 * CRYPTO
 * Provides functions for hashing passwords.
 * Code sourced from https://ciphertrick.com/salt-hash-passwords-using-nodejs-crypto/
 */

import * as crypto from 'crypto';

interface PasswordData {
    salt:string;
    passwordHash:string;
};

/**
 * Generates a random string of the specified length.
 * @param length Length of string to produce.
 * @returns Produced string
 */
function genRandomString(length: number): string {
    return crypto.randomBytes(Math.ceil(length / 2))
        .toString('hex')
        .slice(0, length);
}

/**
 * Hashes the specified password using the specified salt.
 * @param password Plain-text user password
 * @param salt Salt to use for hashing
 * @returns Salt and hashed password
 */
function saltPassword(password: string, salt: string): PasswordData {
    let hash = crypto.createHmac('sha512', salt);
    hash.update(password);
    let value = hash.digest('hex');
    return {
        salt:salt,
        passwordHash:value
    };
}

/**
 * Generates a random salt and a password hash for specified password.
 * @param password Plain-text user password
 * @returns Salt and hashed password
 */
function hashPassword(password: string): PasswordData {
    let salt = genRandomString(16);
    return saltPassword(password, salt);
}

export default {
    hashPassword,
    saltPassword
}