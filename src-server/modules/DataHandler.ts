/**
 * DATA HANDLER
 * Connects to database and provides functions to access database.
 */

import * as sqlite from 'sqlite3';
import * as path from 'path';

// TODO: Determine best way to connect to DB without relying on the Settings module
import Settings from './Settings';

// type of parameters for DB operations
type ParamType = (string | number);

////////////////
///// VARS /////
////////////////

interface RunQueryResult {
    lastID: number;
    changes: number;
}

// Prints calls to DB. For debug purposes.
const SHOW_DB_CALLS = false;

///////////////////////////
///// CALL WHEN READY /////
///////////////////////////

// functions to call when database has been connected to
let callbacks: (()=>void)[] = [ ];
// if the database has been connected to
let ready = false;

// stores function be to called when database has been connected to or immediately
// if already connected
function callWhenReady(callback: ()=>void) {
    if (ready) {
        callback();
    } else {
        callbacks.push(callback);
    }
}

// called when database is connected to
function readyToGo() {
    if (ready) {
        return;
    }

    ready = true;

    if (!callbacks) {
        return;
    }

    for (let cb of callbacks) {
        cb();
    }

    callbacks = null;
}

////////////////////
///// DATABASE /////
////////////////////

// connect to DB
let db = new sqlite.Database(path.join(__dirname, Settings.settings.dbPath), (err)=>{
    if (err) {
        console.error(`Error connecting to database ${Settings.settings.dbPath}: ${err}`);
        process.exit(1);
    }

    readyToGo();
});

/////////////////////////
///// DB OPERATIONS /////
/////////////////////////

// performs a GET statement on database
function queryDBGet(stmt: string, params?: ParamType[] | ParamType): Promise<any> {
    return new Promise((resolve, reject)=>{
        let callback = function(err: Error, row: any) {
            if (err) {
                console.error(`Error doing DB get: ${err}`);
                console.error(stmt);
                reject(err);
            } else {
                resolve(row);
            }
        }

        if (SHOW_DB_CALLS) {
            console.log(`Statement: ${stmt}`);
            console.log(`Params: ${JSON.stringify(params)}`);
        }

        if (params) {
            db.get(stmt, params, callback);
        } else {
            db.get(stmt, callback);
        }
    });
}

// performs an ALL statement on database
function queryDBAll(stmt: string, params?: ParamType[] | ParamType): Promise<any[]> {
    return new Promise((resolve, reject)=>{
        let callback = function(err: Error, rows: any[]) {
            if (err) {
                console.error(`Error doing DB all: ${err}`);
                console.error(stmt);
                reject(err);
            } else {
                resolve(rows);
            }
        }

        if (SHOW_DB_CALLS) {
            console.log(`Statement: ${stmt}`);
            console.log(`Params: ${JSON.stringify(params)}`);
        }

        if (params) {
            db.all(stmt, params, callback);
        } else {
            db.all(stmt, callback);
        }
    });
}

// performs a RUN statement on database
function queryDBRun(stmt: string, params?: ParamType | ParamType[]): Promise<RunQueryResult> {
    return new Promise((resolve, reject)=>{
        let callback = function(this:sqlite.RunResult, err: Error) {
            if (err) {
                console.error(`Error doing DB run: ${err}`);
                console.error(stmt);
                reject(err);
            } else {
                resolve({
                    lastID:this.lastID,
                    changes:this.changes                    
                });
            }
        }

        if (SHOW_DB_CALLS) {
            console.log(`Statement: ${stmt}`);
            console.log(`Params: ${JSON.stringify(params)}`);
        }

        if (params) {
            db.run(stmt, params, callback);
        } else {
            db.run(stmt, callback);
        }
    });
}

function close() {
    return new Promise((resolve, reject)=>{
        db.close((err)=>{
            if (err) {
                console.error(`Error closing database: ${err}`);
                reject();
            } else {
                resolve();
            }
        });
    });
}

export default {
    queryDBGet,
    queryDBAll,
    queryDBRun,
    callWhenReady,
    close
}