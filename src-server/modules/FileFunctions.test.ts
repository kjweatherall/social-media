import FileFunctions from './FileFunctions';

jest.mock('fs');

import * as fs from 'fs';

(fs as any).__setMockFiles([ 'file.txt', 'file(1).txt' ]);

test('ensure filename that does not exist', ()=>{
    expect(FileFunctions.ensureUniqueFilename('test.txt')).toBe('test.txt');
});

test('ensure filename that does exist', ()=>{
    expect(FileFunctions.ensureUniqueFilename('file.txt')).toBe('file(2).txt');
});

test('delete file that exists', ()=>{
    expect(FileFunctions.deleteFile('file.txt')).toBe(true);
});

test('delete file that does not exist', ()=>{
    expect(FileFunctions.deleteFile('test.txt')).toBe(false);
});