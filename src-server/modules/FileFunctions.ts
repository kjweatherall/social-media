import * as fs from 'fs';

const Functions = {
    /**
     * Checks if a filename is already in use and, if it is, adds a number that isn't in use, e.g. file(1).png, file(2).png, etc.
     * @param filename The filename to check
     * @returns { string } A filename that isn't in use
     */
    ensureUniqueFilename(filename: string) {
        // return filename if it isn't already in use
        if (!fs.existsSync(filename)) {
            return filename;
        }

        let period = filename.lastIndexOf('.');
        
        // separate filename base and extension
        if (period !== -1) {
            var ext = filename.substring(period);
            var fn = filename.substring(0, period);
        } else {
            var ext = '';
            var fn = filename;
        }

        // determine if file is already numbered
        if (fn[fn.length - 1] === ')') {
            let open = fn.lastIndexOf('(');
            if (open !== -1) {
                let val = +fn.substring(open, fn.length - 1);
                if (!isNaN(val)) {
                    var idx = val;
                    fn = fn.substring(0, open);
                } else {
                    var idx = 0;
                }
            } else {
                idx = 0;
            }
        } else {
            var idx = 0;
        }

        // check for collisions
        let finalName;
        do {
            finalName = `${fn}(${++idx})${ext}`;
        } while (fs.existsSync(finalName))

        return finalName;
    },
    deleteFile(filePath: string) {        
        console.log(`Deleting ${filePath}`);

        if (!fs.existsSync(filePath)) {
            return false;
        } else {
            try {
                fs.unlinkSync(filePath);
            } catch (err) {
                throw err;
            }

            return true;
        }
    }
};

export default Functions;