import Crypto from './Crypto';

const TEST_PASSWORD = 'this is my test password';

test('hashing password', ()=>{
    const passwordData = Crypto.hashPassword(TEST_PASSWORD);
    expect(typeof passwordData.passwordHash).toEqual('string');
    expect(typeof passwordData.salt).toEqual('string');
});