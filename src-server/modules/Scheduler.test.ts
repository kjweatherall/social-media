import Scheduler from './Scheduler';

afterAll(()=>{
    Scheduler.stop();
});

test('If Scheduler is running immediately', ()=>{
    expect(Scheduler.isRunning()).toBe(true);
});

test('Adding task to scheduler', ()=>{
    expect(Scheduler.addTask(()=>{}, 1)).toBe(1);
});

test('Stopping scheduler', ()=>{
    expect(Scheduler.stop()).toBe(undefined);
});

test('If Scheduler has been stopped', ()=>{
    expect(Scheduler.isRunning()).toBe(false);
});

test('Starting scheduler', ()=>{
    expect(Scheduler.start()).toBe(undefined);
});

test('If Scheduler is running again', ()=>{
    expect(Scheduler.isRunning()).toBe(true);
});

test('Removing task from scheduler', ()=>{
    expect(Scheduler.removeTask(1)).toBe(true);
});