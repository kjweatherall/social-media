/**
 * UPLOAD FUNCTIONS
 * Provides functions for the receiving of uploaded images.
 */

import * as formidable from 'formidable';
import * as express from 'express';
import * as fs from 'fs';
import * as path from 'path';

import DataHandler from '../modules/DataHandler';
import DataHelpers from '../data-helpers';

import MediaFunctions from '../modules/MediaFunctions';
import FileFunctions from '../modules/FileFunctions';

// common folders
const DATA_FOLDER = path.join(__dirname, '../../data');

const TEMP_FOLDER = path.join(DATA_FOLDER, 'temp');
const PROFILES_FOLDER = path.join(DATA_FOLDER, 'profiles');
const UPLOADS_FOLDER = path.join(DATA_FOLDER, 'uploads');
const BANNERS_FOLDER = path.join(DATA_FOLDER, 'banners');

const THUMBNAILS_FOLDER = path.join(DATA_FOLDER, 'thumbnails');

const PROFILES_THUMBNAILS_FOLDER = path.join(THUMBNAILS_FOLDER, 'profiles');
const BANNER_THUMBNAILS_FOLDER = path.join(THUMBNAILS_FOLDER, 'banners');

// create necessary folders
if (!fs.existsSync(TEMP_FOLDER)) {
    fs.mkdirSync(TEMP_FOLDER);
}
if (!fs.existsSync(PROFILES_FOLDER)) {
    fs.mkdirSync(PROFILES_FOLDER);
}
if (!fs.existsSync(BANNERS_FOLDER)) {
    fs.mkdirSync(BANNERS_FOLDER);
}
if (!fs.existsSync(UPLOADS_FOLDER)) {
    fs.mkdirSync(UPLOADS_FOLDER);
}
if (!fs.existsSync(THUMBNAILS_FOLDER)) {
    fs.mkdirSync(THUMBNAILS_FOLDER);
}
if (!fs.existsSync(PROFILES_THUMBNAILS_FOLDER)) {
    fs.mkdirSync(PROFILES_THUMBNAILS_FOLDER);
}
if (!fs.existsSync(BANNER_THUMBNAILS_FOLDER)) {
    fs.mkdirSync(BANNER_THUMBNAILS_FOLDER);
}


// fields that can be included with uploads
type Fields = { text:string };

export enum UploadFailure {
    GENERIC_ERROR = 1, UPLOAD_ABORTED, TRANSFER_ERROR, UNSUPPORTED_FORMAT, IMAGE_ISSUE
}

const SUPPORTED_EXT = ['jpg','jpeg','gif','png','bmp'];

// extracts data and images from an upload request
function parseUpload(req: express.Request): Promise<{file:formidable.File,fields:Fields}> {
    return new Promise((resolve, reject)=>{
        let form = new formidable.IncomingForm();
        form.uploadDir = TEMP_FOLDER;
        form.keepExtensions = true;
        form.multiples = false;

        // get files and data from upload
        form.parse(req, async (err, fields, files)=>{
            if (err) {
                console.error(`Error parsing upload: ${err}`);
                reject({error:UploadFailure.GENERIC_ERROR});
            } else {
                // handle file, if any
                if (files.file) {
                    try {
                        // determine file type to see if it is allowed
                        var ext = await MediaFunctions.determineExtension(files.file.path);
                    } catch (err) {
                        if (err) {
                            console.error(`Error determining file extension: ${err}`);
                        }

                        if (fs.existsSync(files.file.path)) {
                            fs.unlinkSync(files.file.path);
                        }

                        return reject({error:UploadFailure.IMAGE_ISSUE});
                    }

                    // check extension is allowed
                    if (!SUPPORTED_EXT.includes(ext)) {
                        if (fs.existsSync(files.file.path)) {
                            fs.unlinkSync(files.file.path);
                        }

                        return reject({error:UploadFailure.UNSUPPORTED_FORMAT});
                    }

                    // if file is bmp, convert to png
                    if (ext === 'bmp') {
                        let file = files.file.path;

                        // determine file directory
                        let pIdx = file.lastIndexOf('\\');
                        if (pIdx === -1) {
                            pIdx = file.lastIndexOf('/');
                        }
                        let p = file.substring(0, pIdx + 1);

                        // determine filename for converted image
                        let f = file.substring(pIdx + 1);
                        let fIdx = f.lastIndexOf('.');
                        if (fIdx !== -1) {
                            var output = `${p}${f.substring(0, fIdx)}.png`;
                        } else {
                            var output = `${p}${f}.png`;
                        }

                        // make sure filename isn't in use
                        output = FileFunctions.ensureUniqueFilename(output);

                        // convert image
                        try {
                            await MediaFunctions.convertImage(file, output);
                        } catch (err) {
                            if (err) {
                                console.error(`Error converting BMP image: ${err}`);
                            }
                            if (fs.existsSync(files.file.path)) {
                                fs.unlinkSync(files.file.path);
                            }
                            return reject({error:UploadFailure.IMAGE_ISSUE});
                        }

                        // remove old image
                        try {
                            fs.unlinkSync(files.file.path);
                        } catch (err) {
                            console.error(`Error removing old image: ${err}`);
                        }

                        // point data to new filename
                        files.file.path = output;

                        // change name
                        files.file.name = `${f}.png`;
                    }
                }

                resolve({
                    file:files.file,
                    fields:fields as Fields
                });
            }
        });

        form.on('aborted', ()=>{
            console.warn('Upload aborted');
            reject({error:UploadFailure.UPLOAD_ABORTED});
        });

        form.on('error', err=>{
            req.destroy(Error('Error transferring file.'))
            console.error(`Error uploading file: ${err}`);
            reject({error:UploadFailure.TRANSFER_ERROR});
        });

        // form.on('end', ()=>{
            
        // });
    });
}

const Functions = {
    Accounts:{
        // uploads a profile image for an account
        async uploadPhoto(accountID: number, req: express.Request) {
            // get file
            try {
                var results = await parseUpload(req);
            } catch (err) {
                throw err;
            }
            
            // don't do anything if no file provided
            if (!results.file) {
                return null;
            }

            // make thumbnails
            let filenameThumbnailLarge = `${accountID}_large.jpg`;
            let filenameThumbnailSmall = `${accountID}_small.jpg`;
            let ret = await Promise.all([
                DataHandler.queryDBGet(`SELECT profile_image,profile_image_thumbnail_small,profile_image_thumbnail_large FROM accounts WHERE accountID=${accountID}`),
                MediaFunctions.makeImageThumbnail(results.file.path, path.join(PROFILES_THUMBNAILS_FOLDER, filenameThumbnailLarge), 132, 132),
                MediaFunctions.makeImageThumbnail(results.file.path, path.join(PROFILES_THUMBNAILS_FOLDER, filenameThumbnailSmall), 80, 80)
            ]);

            const profile = ret[0];

            // determine file extension of file
            let ext = results.file.name.substring(results.file.name.lastIndexOf('.') + 1);

            // put new profile image in the correct place
            let filename = `${accountID}.${ext}`;
            fs.renameSync(results.file.path, path.join(PROFILES_FOLDER, filename));

            // determine if any changes need to be made to database
            let changes: string[] = [ ];
            let params: string[] = [ ];

            if (profile.profile_image !== filename) {
                changes.push('profile_image=?');
                params.push(filename);
            }
            if (profile.profile_image_thumbnail_large !== filenameThumbnailLarge) {
                changes.push('profile_image_thumbnail_large=?');
                params.push(filenameThumbnailLarge);
            }
            if (profile.profile_image_thumbnail_small !== filenameThumbnailSmall) {
                changes.push('profile_image_thumbnail_small=?');
                params.push(filenameThumbnailSmall);
            }

            // apply any necessary changes
            if (changes.length > 0) {
                await DataHandler.queryDBRun(`UPDATE accounts SET ${changes.join(',')} WHERE accountID=${accountID}`, params);
            }

            // return filenames
            return {
                image:filename,
                thumbnailLarge:filenameThumbnailLarge,
                thumbnailSmall:filenameThumbnailSmall
            };
        },
        // uploads a banner image for an account
        async uploadBanner(accountID: number, req: express.Request) {
            // get file
            try {
                var results = await parseUpload(req);
            } catch (err) {
                throw err;
            }
            
            // don't do anything if no file provided
            if (!results.file) {
                return null;
            }

            // make thumbnail
            let filenameThumbnail = `${accountID}.jpg`;
            let ret = await Promise.all([
                DataHandler.queryDBGet(`SELECT banner_image,banner_image_thumbnail FROM accounts WHERE accountID=${accountID}`),
                MediaFunctions.makeImageThumbnail(results.file.path, path.join(BANNER_THUMBNAILS_FOLDER, filenameThumbnail), 600, 200),
            ]);

            const profile = ret[0];

            // determine file extension of file
            let ext = results.file.name.substring(results.file.name.lastIndexOf('.') + 1);

            // put new banner image in the correct place
            let filename = `${accountID}.${ext}`;
            fs.renameSync(results.file.path, path.join(BANNERS_FOLDER, filename));

            // determine if any changes need to be made to database
            let changes: string[] = [ ];
            let params: string[] = [ ];

            if (profile.profile_image !== filename) {
                changes.push('banner_image=?');
                params.push(filename);
            }
            if (profile.banner_image_thumbnail !== filenameThumbnail) {
                changes.push('banner_image_thumbnail=?');
                params.push(filenameThumbnail);
            }

            // apply any necessary changes
            if (changes.length > 0) {
                await DataHandler.queryDBRun(`UPDATE accounts SET ${changes.join(',')} WHERE accountID=${accountID}`, params);
            }

            // return filenames
            return {
                image:filename,
                thumbnail:filenameThumbnail
            };
        }        
    },    
    Posts:{
        // adds a post
        async add(req: express.Request, accountID: number) {
            // get file and/or data
            let results = await parseUpload(req);

            // if no file and no data, do nothing
            if (!results.file && !results.fields.text) {
                throw 'No text or file specified.';
            }

            // add post
            let postResults = await DataHelpers.Posts.create(accountID, results.fields.text, results.file ? [ results.file.path ] : undefined);

            if (results.file) {
                // add thumbnail record
                let thumbnail = await DataHandler.queryDBRun('INSERT INTO thumbnails(extension) VALUES("jpg")');
                // add thumbnail ID to post record
                await DataHandler.queryDBRun(`UPDATE posts_images SET thumbnailID=${thumbnail.lastID} WHERE postID=${postResults.post.postID}`);
                // create thumbnail
                await MediaFunctions.makeImageThumbnail(results.file.path, path.join(THUMBNAILS_FOLDER, `${thumbnail.lastID}.jpg`), 500, 500);

                postResults.post.thumbnails = `${thumbnail.lastID}.jpg`;

                // determine file extension
                let ext = results.file.name.substring(results.file.name.lastIndexOf('.') + 1);
                // move file to to proper location with proper name
                fs.renameSync(results.file.path, path.join(UPLOADS_FOLDER, `${postResults.images[0]}.${ext}`));
                // add image reference to post data
                postResults.post.images = `${postResults.images[0]}.${ext}`;
            }

            // return post data
            return postResults.post;
        }
    }
};

export default Functions;