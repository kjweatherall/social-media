/**
 * MAINTAINER
 * The Maintainer module is intended to handle tasks that are repeated.
 */

// types
import { Account } from '../ServerInterfaces';

// modules
import DataHandler from '../modules/DataHandler';
import Scheduler from '../modules/Scheduler';
import APIFunctions from './APIFunctions';
import Settings from '../modules/Settings';

// duration (in minutes) of temporary accounts
const ACCOUNT_DURATION = 60;

// performs maintenance
async function deleteExpiredAccounts() {    
    let now = new Date().getTime(),
        expiry = now - (1000 * 60 * ACCOUNT_DURATION);

    try {
        var accounts: Account[] = await DataHandler.queryDBAll(`SELECT * FROM accounts WHERE type=0 AND created_at NOT NULL AND created_at < ${expiry}`)
    } catch (err) {
        if (err) {
            console.error(`Error getting expired accounts: ${err}`);
        }
        return;
    }
    
    if (Settings.settings.debug) {
        console.log(`Removing ${accounts.length} expired accounts`);
    }

    for (let acc of accounts) {
        APIFunctions.Accounts.remove(acc.accountID);
    }
}

// schedule maintenance
export default ()=>{
    // schedule deletion of expired accounts
    Scheduler.addTask(deleteExpiredAccounts, ACCOUNT_DURATION);
};