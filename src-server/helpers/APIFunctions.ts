/**
 * API FUNCTIONS
 * A module that is intended to sit between the API endpoints and the database functions to ensure proper credentials for accessing
 * or modifying data. Should also prevent sensitive data - such as passwords - from being sent back for external requests.
 * 
 * This was envisioned as a way to prevent sensitive data from being improperly accessed or modified, by requiring appropriate
 * credentials to be submitted when accessing functions so that credential checking wouldn't be handled manually, which would
 * open the door for bugs.
 * 
 * But the intended functionality has not yet been implemented. Instead, this module is largely used to call data functions and 
 * process any returned data before returning it to the API endpoint function that called it. Authentication is being handled
 * manually by the API endpoints.
 */

import * as express from 'express';
import * as fs from 'fs';
import * as path from 'path';

// types
import { Account, AccountsUpdate } from '../ServerInterfaces';
import { PostsQuery, Post, AutocompleteResults, SearchResults, ProfileBasic, Profile, PostResults } from '../../shared/SharedInterfaces';
import { SQLProfile } from '../data-helpers/modules/Profiles';
import { SQLAccount } from '../data-helpers/modules/Accounts';

// modules
import DataHandler from '../modules/DataHandler';
import DataFunctions from '../data-helpers';
import UploadFunctions from './UploadFunctions';
import { PostsResults } from '../data-helpers/modules/Posts';
import FileFunctions from '../modules/FileFunctions';

let Functions = {
    Accounts:{
        get(accountID: number): Promise<Account | Account[]> {
            return DataFunctions.Accounts.read({accountID});
        },
        update(accountID: number, details: AccountsUpdate) {
            return DataFunctions.Accounts.update(accountID, details);
        },
        uploadPhoto(accountID: number, req: express.Request) {
            return UploadFunctions.Accounts.uploadPhoto(accountID, req);
        },
        uploadBanner(accountID: number, req: express.Request) {
            return UploadFunctions.Accounts.uploadBanner(accountID, req);
        },
        async remove(accountID: number) {
            let imageData = await Promise.all([
                Functions.Accounts.get(accountID),
                DataHandler.queryDBAll(`SELECT imageID,posts_images.extension AS image_extension,thumbnailID,thumbnails.extension AS thumbnail_extension FROM posts LEFT JOIN posts_images USING(postID) LEFT JOIN thumbnails USING(thumbnailID) WHERE accountID=${accountID} AND imageID NOT NULL`)
            ]);

            let account = imageData[0] as Account;
            const DATA_FOLDER = '../../data/';

            // delete profile image
            if (account.profileImage) {
                try {
                    FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'profiles', account.profileImage));
                } catch (err) {
                    console.error(err);
                }
            }

            // delete profile image thumbnail large
            if (account.profileImageThumbnailLarge) {
                try {
                    FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'thumbnails/profiles', account.profileImageThumbnailLarge));
                } catch (err) {
                    console.error(err);
                }
            }

            // delete profile image thumbnail small
            if (account.profileImageThumbnailSmall) {
                try {
                    FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'thumbnails/profiles', account.profileImageThumbnailSmall));
                } catch (err) {
                    console.error(err);
                }
            }

            // delete banner image
            if (account.bannerImage) {
                try {
                    FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'banners', account.bannerImage));
                } catch (err) {
                    console.error(err);
                }
            }

            // delete banner image thumbnail
            if (account.bannerImage) {
                try {
                    FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'thumbnails/banners', account.bannerImageThumbnail));
                } catch (err) {
                    console.error(err);
                }
            }

            // delete post images
            let posts: {imageID:number,image_extension:string,thumbnailID:number,thumbnail_extension:string}[] = imageData[1];
            for (let i = 0; i < posts.length; i++) {
                if (posts[i].imageID) {
                    try {
                        FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'uploads', `${posts[i].imageID}.${posts[i].image_extension}`));
                    } catch (err) {
                        console.error(`Error deleting post image: ${err}`);
                    }
                }

                if (posts[i].thumbnailID) {
                    try {
                        FileFunctions.deleteFile(path.join(__dirname, DATA_FOLDER, 'thumbnails', `${posts[i].thumbnailID}.${posts[i].thumbnail_extension}`));
                    } catch (err) {
                        console.error(`Error deleting `);
                    }
                }
            }

            // delete all data in database
            let postIDs = (await DataHandler.queryDBAll(`SELECT postID FROM posts WHERE accountID=${accountID}`)).map(p=>p.postID).join(',');
            let thumbnailIDs = posts.map(id=>id.thumbnailID).join(',');

            await Promise.all([
                DataHandler.queryDBRun(`DELETE FROM accounts WHERE accountID=${accountID}`),
                DataHandler.queryDBRun(`DELETE FROM posts WHERE accountID=${accountID}`),
                DataHandler.queryDBRun(`DELETE FROM accounts_posts WHERE accountID=${accountID}`),
                DataHandler.queryDBRun(`DELETE FROM followers WHERE accountID=${accountID} OR followingID=${accountID}`),
                DataHandler.queryDBRun(`DELETE FROM posts_images WHERE postID IN (${postIDs})`),
                DataHandler.queryDBRun(`DELETE FROM thumbnails WHERE thumbnailID IN (${thumbnailIDs})`),
                DataHandler.queryDBRun(`DELETE FROM posts_tags WHERE postID IN (${postIDs})`),
                DataHandler.queryDBRun(`DELETE FROM posts_likes WHERE accountID=${accountID} OR postID IN (${postIDs})`),
                DataHandler.queryDBRun(`DELETE FROM accounts_sessions WHERE accountID=${accountID}`)
            ]);
        }
    },
    Posts:{
        query(query: PostsQuery, accountID?: number) {
            return DataFunctions.Posts.read(query, accountID);
        },
        add(req: express.Request, accountID: number) {
            return UploadFunctions.Posts.add(req, accountID);
        },
        get(postIDs: number | number[]) {
            return DataFunctions.Posts.read({postIDs});
        },
        async delete(streamID: number) {
            // get post
            let stream = await DataHandler.queryDBGet(`SELECT postID,repost FROM accounts_posts WHERE streamID=${streamID}`);

            // post not found; do nothing
            if (!stream) {
                throw 'No stream for stream ID';
            }
            
            // delete images if no a repost
            if (!stream.repost) {
                let postID = stream.postID;
                let post = await DataFunctions.Posts.read({postIDs:postID}) as Post;

                if (post.images) {
                    let images = post.images.split(',')

                    for (let i = 0; i < images.length; i++) {
                        let p = path.join(__dirname, `../../data/uploads/${images[i]}`);
                        if (fs.existsSync(p)) {
                            fs.unlinkSync(p);
                        }
                    }

                    let thumbnails = post.thumbnails.split(',');

                    for (let i = 0; i < thumbnails.length; i++) {
                        let p = path.join(__dirname, `../../data/thumbnails/${thumbnails[i]}`);
                        if (fs.existsSync(p)) {
                            fs.unlinkSync(p);
                        }
                    }
                }
            }

            await DataFunctions.Posts.remove(streamID);
        },
        like(accountID: number, postID: number) {
            return DataFunctions.Posts.like(accountID, postID);
        },
        unlike(accountID: number, postID: number) {
            return DataFunctions.Posts.unlike(accountID, postID);
        },
        getLikes(postID: number) {
            return DataFunctions.Posts.getLikes(postID);
        },
        repost(accountID: number, postID: number) {
            return DataFunctions.Posts.repost(accountID, postID);
        },
        unrepost(accountID: number, postID: number) {
            return DataFunctions.Posts.unrepost(accountID, postID);
        }
    },
    Search:{
        async get(query: string, accountID: number, maxResults = 20,criteria?:{time:'before'|'after',value:number}): Promise<SearchResults | PostResults> {
            if (criteria) {
                return await DataFunctions.Posts.read({text:{value:query},maxResults:maxResults, publishedAt:{time:criteria.value,relativeToTime:criteria.time === 'before' ? 'before' : 'after'}},accountID) as PostResults;
            }

            let results = await Promise.all([
                DataHandler.queryDBAll(`SELECT accounts.*,(SELECT count(*) FROM followers WHERE followingID=accounts.accountID) AS followers, (SELECT count(*) FROM followers WHERE accountID=${accountID} AND followingID=accounts.accountID) AS following FROM accounts ` +
                `WHERE (display_name LIKE ? OR handle LIKE ?) AND (accounts.type!=0 OR accountID=${accountID})`, [`%${query}%`,`%${query}%`]),
                DataFunctions.Posts.read({text:{value:query},maxResults:maxResults},accountID)
            ]); 

            return {
                profiles:(results[0]).map((p:Account&SQLProfile)=>({
                    bannerImage:p.banner_image,
                    bannerImageThumbnail:p.banner_image_thumbnail,
                    bio:p.bio,
                    connection:(p.accountID === accountID ? 'self' : p.following ? 'following' : 'unconnected'),
                    displayName:p.display_name,
                    followers:p.followers,
                    following:p.following,
                    handle:p.handle,
                    posts:p.posts,
                    profileImage:p.profile_image,
                    profileImageThumbnailLarge:p.profile_image_thumbnail_large,
                    profileImageThumbnailSmall:p.profile_image_thumbnail_small
                } as Profile)),
                posts:(results[1] as PostsResults)
            }
        }
    },
    Profiles:{
        async get(accountID: number, requesterID?: number) {
            return DataFunctions.Profiles.get(accountID, requesterID);
        }
    },
    Followers:{
        follow(accountID: number, followingID: number) {
            return DataFunctions.Followers.follow(accountID, followingID);
        },
        unfollow(accountID: number, followingID: number) {
            return DataFunctions.Followers.unfollow(accountID, followingID);
        },
        get(accountID: number, requesterID?: number, maxResults?: number) {
            return DataFunctions.Followers.getFollowersAndFollowing(accountID, requesterID, maxResults);
        }
    },
    Autocomplete:{
        async get(query: string, accountID?: number): Promise<AutocompleteResults> {
            query = query.trim();

            // accounts
            {
                var paramsAccounts: string[] = [ ];

                var stmtAccounts = 'SELECT * FROM accounts WHERE (handle LIKE ?'; 
                
                if (query[0] === '@') {
                    let name = query.substring(1);
                    paramsAccounts.push(`${name}%`);
                } else {
                    stmtAccounts += ' OR display_name LIKE ?';
                    paramsAccounts.push(`%${query}%`);
                    paramsAccounts.push(`%${query}%`);
                }
                
                if (accountID) {
                    stmtAccounts += `) AND (type != 0 OR accountID=${accountID})`;
                } else {
                    stmtAccounts += `) AND type!=0`;
                }

                stmtAccounts += ' LIMIT 5';

                console.log(stmtAccounts);
            }

            // hashtags
            {
                var paramsHashtags: string[] = [ ];

                if (query[0] === '#') {
                    let name = query.substring(1);
                    var stmtHashtags = `SELECT * FROM tags WHERE tag LIKE ?`;
                    paramsHashtags.push(`${name}%`);
                } else {
                    var stmtHashtags = `SELECT * FROM tags WHERE tag LIKE ?`;
                    paramsHashtags.push(`%${query}%`);
                }
                
                stmtHashtags += ' LIMIT 5';
            }

            let results = await Promise.all([
                await DataHandler.queryDBAll(stmtAccounts, paramsAccounts),
                await DataHandler.queryDBAll(stmtHashtags, paramsHashtags)
            ])

            return {
                accounts:results[0].map((a:SQLAccount)=>({
                    displayName:a.display_name,
                    handle:a.handle,
                    profileImage:a.profile_image,
                    profileImageThumbnailLarge:a.profile_image_thumbnail_large,
                    profileImageThumbnailSmall:a.profile_image_thumbnail_small,
                    bannerImage:a.banner_image,
                    bannerImageThumbnail:a.banner_image_thumbnail,
                    bio:a.bio
                } as ProfileBasic)),
                hashtags:results[1].map((t:{tag:string})=>t.tag)
            }
        }
    }
};

export default Functions;