import * as express from 'express';
import * as path from 'path';
import * as fs from 'fs';

// types
import { Account } from '../ServerInterfaces';

// modules
import DataHelper from '../data-helpers';
import Accounts from '../data-helpers/modules/Accounts';
import APIFunctions from '../helpers/APIFunctions';
import Settings from '../modules/Settings';

const router = express.Router();

/* LOG IN/OUT */

router.route('/login')
.get((req, res)=>{
    if (res.locals.loggedIn) {
        res.redirect('/');
    } else {
        res.sendFile(path.join(__dirname, '../../../pages/login.html'));
    }
})
.post((req, res)=>{
    // if already logged in, no need to login
    if (res.locals.loggedIn) {
        res.redirect('/');
        return;
    }

    const username = req.body.username,
        password = req.body.password;

    if (!username) {
        // username required; if none, login failed
        res.redirect('/login');
    } else {
        // check login details
        Accounts.login(username, password)
        .then(accountID=>{
            if (accountID) {
                // login details are correct; generate new session code
                Accounts.generateSessionCode(accountID)
                .then(sessionCode=>{
                    req.session.accountID = accountID;
                    req.session.sessionCode = sessionCode;
                    res.redirect('/');
                })
                .catch(err=>{
                    console.error(`Error generating session code: ${err}`);
                    res.status(500).end();
                })
            } else {
                // login details incorrect; go back
                res.redirect(`/login?username=${username}`);
            }
        })
        .catch(err=>{
            if (err) {
                console.error(`Error logging in: ${err}`);
            }
            res.status(500).end();
        });
    }
});

router.get('/logout', async (req, res)=>{    
    if (req.session.accountID) {                
        let account = await DataHelper.Accounts.read({accountID:req.session.accountID}) as Account;

        if (account.type === 0) {
            try {
                await APIFunctions.Accounts.remove(req.session.accountID)
            } catch (err) {
                if (err) {
                    console.error(`Error removing account: ${err}`);
                }
            }
        } else {
            try {
                await DataHelper.Accounts.clearSessionCode(req.session.accountID);
            } catch (err) {
                if (err) {
                    console.error(`Error clearing session code: ${err}`);
                }
            }
        }

        req.session.accountID = 0;
    } else {

    }

    if (req.session.sessionCode) {
        req.session.sessionCode = '';
    }

    res.redirect('/login');
});

/* CREATE ACCOUNT */

router.post('/create', (req, res)=>{
    const displayName = req.body.displayName,
        handle = req.body.handle,
        username = req.body.username,
        password = req.body.password,
        accessCode = req.body.accessCode;

    // check for missing account details
    let missing = false;

    missing = !displayName || !handle || !username || !accessCode;

    // check for required details    
    if (missing || accessCode !== Settings.settings.accessCode) {
        res.redirect(`/login?action=create&username=${username}&displayName=${displayName}&handle=${handle}`);
    } else {
        DataHelper.Accounts.create(displayName, handle, username, password, 1)
        .then(accountID=>{
            if (accountID) {
                // account creation successful; generate session code
                Accounts.generateSessionCode(accountID)
                .then(sessionCode=>{
                    req.session.accountID = accountID;
                    req.session.sessionCode = sessionCode;
                    res.redirect('/');
                })
                .catch(err=>{
                    console.error(`Error generating session code: ${err}`);
                    res.redirect(`/login?action=create&username=${username}&displayName=${displayName}&handle=${handle}`);
                });
            } else {
                // account creation failed for some reason; go back
                console.warn("Failed without error to create account.");
                res.redirect(`/login?action=create&username=${username}&displayName=${displayName}&handle=${handle}`);
            }
        })
        .catch(err=>{
            if (err) {
                console.error(err);
            }
            res.status(500).end();
        })
    }
});

/* GUEST ACCOUNTS */
router.post('/guest', (req, res)=>{
    if (res.locals.loggedIn) {
        return res.redirect('/');
    }

    Accounts.createGuest()
    .then(accountID=>{
        Accounts.generateSessionCode(accountID)
        .then(sessionCode=>{
            req.session.accountID = accountID;
            req.session.sessionCode = sessionCode;
            res.redirect('/');
        })
        .catch(err=>{
            console.error(`Error generating session code: ${err}`);
            res.status(500).end('Server error');
        });
    })
    .catch(err=>{
        console.error(`Error creating guest account: ${err}`);
        res.status(500).end('Server error');
    });
});

/* IMAGES */

// use route for images rather than using public folder so we can return default image if specific image is missing
// TODO: Determine if there is a way to do this use express.static
router.get(['/images/:folder/:filename', '/images/:folder/:subfolder/:filename' ], (req, res)=>{
    let folder = req.params.folder,
        subfolder = req.params.subfolder;

    let folderPath = path.join(__dirname, '../../data', folder);
    if (subfolder) {
        folderPath = path.join(folderPath, subfolder);
    }

    let filePath = path.join(folderPath, req.params.filename);

    if (fs.existsSync(filePath)) {
        res.sendFile(filePath);
    } else {
        console.error('Image not found: ' + filePath);
        if (folder === 'profiles') {
            res.sendFile(path.join(__dirname, '../../../public/profile.jpg'));
        } else if (folder === 'banners') {
            res.sendFile(path.join(__dirname, '../../../public/banner.jpg'));
        } else {
            res.status(404).end();
        }
    }
});

/* DEFAULT */
router.get(['/','/user/:handle','/search/:search','/user/:handle/followers'], (req, res)=>{
    if (!res.locals.loggedIn) {
        res.redirect('/login');
    } else {
        res.sendFile(path.join(__dirname, '../../../pages/index.html'));
    }
});

export default router;