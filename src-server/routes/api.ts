/* Directs each endpoint to the correct API function.
 */

import * as express from 'express';

// types
import { Post } from '../../shared/SharedInterfaces';

// modules
import DataHandler from '../modules/DataHandler';
import APIFunctions from '../helpers/APIFunctions';
import { UploadFailure } from '../helpers/UploadFunctions';
import Settings from '../modules/Settings';
import Posts from '../data-helpers/modules/Posts';

const router = express.Router();

/////////////////
///// DEBUG /////
/////////////////

// prints out API calls
if (Settings.settings.debug) {
    router.use((req, res, next)=>{
        console.log(`${req.method}: ${req.url}`);
        next();
    });
}

/////////////////////
///// FUNCTIONS /////
/////////////////////

// prints an error if there is one and returns 500 response
function onFailure(res: express.Response, err?: any) {
    if (err) {
        console.error(err);
    }
    res.status(500).end();
}

// gets account ID associated with a handle
function getAccountID(handle: string): Promise<number> {
    return new Promise((resolve, reject)=>{
        if (!handle) {
            resolve(null);
        } else {
            DataHandler.queryDBGet('SELECT accountID FROM accounts WHERE handle=?', handle)
            .then(account=>{
                if (account) {
                    resolve(+account.accountID);
                } else {
                    resolve(null);
                }
            })
            .catch(err=>{
                console.error(`Error getting account ID for handle: ${err}`);
                reject();
            });
        }
    });
}

// gets your content and the content of those you follow
const getPostsFunc = async (req: express.Request, res: express.Response)=>{
    let handle = req.params.handle;

    if (!handle) {
        if (!res.locals.loggedIn) {
            return res.status(403).end();
        }
    } else {
        try {
            var accountID = await getAccountID(handle);
        } catch (err) {
            return onFailure(res, err);
        }

        if (!accountID) {
            return res.status(404).end();
        }
    }

    var time: {time:number,relativeToTime:'before'|'after'|'equals',includeTime:boolean} = null;

    if (req.query.before) {
        time = {
            time:+req.query.before,
            relativeToTime:'before',
            includeTime:false
        };
    } else if (req.query.after) {
        time = {
            time:+req.query.after,
            relativeToTime:'after',
            includeTime:false
        };
    }

    APIFunctions.Posts.query({
            accountID:handle?accountID:req.session.accountID,
            own:handle === undefined,
            maxResults:20,
            publishedAt:time
        }, req.session.accountID
    )
    .then(posts=>{
        res.json(posts)
    })
    .catch(err=>onFailure(res, err));
}

////////////////////
///// ACCOUNTS /////
////////////////////

// gets user details for logged-in user
router.get('/users', (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(401).end();
    }

    APIFunctions.Profiles.get(req.session.accountID)
    .then(profile=>res.json(profile))
    .catch(err=>onFailure(res, err));
});

// gets user details for specified handle
router.route('/users/:handle')
.get(async (req, res)=>{
    try {
        var accountID = await getAccountID(req.params.handle);
    } catch (err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    }

    APIFunctions.Profiles.get(accountID, req.session.accountID)
    .then(profile=>res.json(profile))
    .catch(err=>onFailure(res, err));
})
// updates user details for specified handle
.put(async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    try {
        var accountID = await getAccountID(req.params.handle);
    } catch(err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    } else if (accountID !== req.session.accountID) {
        return res.status(403).end();
    }

    APIFunctions.Accounts.update(accountID, req.body)
    .then(()=>res.status(204).end())
    .catch(err=>onFailure(res, err));
});

// changes profile image for handle
router.post('/users/:handle/photo', async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    try {
        var accountID = await getAccountID(req.params.handle);
    } catch(err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    } else if (accountID !== req.session.accountID) {
        return res.status(403).end();
    }

    APIFunctions.Accounts.uploadPhoto(accountID, req)
    .then(profilePhoto=>{
        res.json(profilePhoto);
    })
    .catch(err=>onFailure(res, err));
});

// changes banner for handle
router.post('/users/:handle/banner', async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    try {
        var accountID = await getAccountID(req.params.handle);
    } catch(err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    } else if (accountID !== req.session.accountID) {
        return res.status(403).end();
    }

    APIFunctions.Accounts.uploadBanner(accountID, req)
    .then(bannerImage=>res.json(bannerImage))
    .catch(err=>onFailure(res, err));
});

router.route('/users/:handle/follow')
// follows a user
.post(async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    try {
        var accountID = await getAccountID(req.params.handle);
    } catch(err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    }

    APIFunctions.Followers.follow(req.session.accountID, accountID)
    .then(()=>res.end())
    .catch(err=>onFailure(res, err));
})
// unfollows a user
.delete(async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    try {
        var accountID = await getAccountID(req.params.handle);
    } catch(err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    }

    APIFunctions.Followers.unfollow(req.session.accountID, accountID)
    .then(()=>res.end())
    .catch(err=>onFailure(res, err));
});

// gets users being followed by and following handle
router.get('/users/:handle/followers', async (req, res)=>{
    try {
        var accountID = await getAccountID(req.params.handle);
    } catch (err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    }

    APIFunctions.Followers.get(accountID, req.session.accountID)
    .then(followers=>res.json(followers))
    .catch(err=>onFailure(res, err));
});

/////////////////
///// POSTS /////
/////////////////

// deletes a post
router.delete('/posts/:streamID', async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    let streamID = +req.params.streamID;

    if (isNaN(streamID) || streamID <= 0) {
        return res.status(400).end("Post ID is not a valid number.");
    }

    let post = await DataHandler.queryDBGet(`SELECT accountID FROM accounts_posts WHERE streamID=${streamID}`);

    if (!post) {
        return res.status(404).end('Post not found');
    } else if (post.accountID !== req.session.accountID) {
        return res.status(403).end();
    }

    APIFunctions.Posts.delete(streamID)
    .then(()=>res.status(204).end())
    .catch(err=>onFailure(res, err));
});

router.route('/posts/:postID/likes')
// gets likes for post
.get(async (req, res)=>{
    let postID = +req.params.postID;

    if (isNaN(postID) || postID <= 0) {
        return res.status(400).end('Post ID is not a valid number.');
    }

    let post = await APIFunctions.Posts.get(postID) as Post;

    if (!post) {
        return res.status(404).end();
    }

    APIFunctions.Posts.getLikes(postID)
    .then(likes=>res.json(likes))
    .catch(err=>onFailure(res, err));
})
// likes a post
.post(async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    let postID = +req.params.postID;

    if (isNaN(postID) || postID <= 0) {
        return res.status(400).end('Post ID is not a valid number.');
    }

    let post = await APIFunctions.Posts.get(postID) as Post;

    if (!post) {
        return res.status(404).end();
    }

    APIFunctions.Posts.like(req.session.accountID, +req.params.postID)
    .then(()=>res.end())
    .catch(err=>onFailure(res, err));
})
// unlikes a post
.delete(async (req, res)=>{
    if (!res.locals.loggedIn) {
        return res.status(403).end();
    }

    let postID = +req.params.postID;

    if (isNaN(postID) || postID <= 0) {
        return res.status(400).end('Post ID is not a valid number.');
    }

    let post = await APIFunctions.Posts.get(postID) as Post;

    if (!post) {
        return res.status(404).end();
    }

    APIFunctions.Posts.unlike(req.session.accountID, postID)
    .then(()=>res.end())
    .catch(err=>onFailure(res, err));
});

router.route('/posts/:postID/repost')
.post(async (req, res)=>{
    let postID = +req.params.postID;

    if (isNaN(postID)) {
        return res.status(400).end('Post ID is not a number');
    }

    let post = await Posts.read({postIDs:postID}) as Post;
    if (!post) {
        return res.status(404).end();
    }

    try {
        await APIFunctions.Posts.repost(req.session.accountID, postID);
    } catch (err) {
        return res.status(500).end();
    }

    res.status(204).end();
})
.delete(async (req, res)=>{
    let postID = +req.params.postID;

    if (isNaN(postID)) {
        return res.status(400).end('Post ID is not a number');
    }

    let post = await Posts.read({postIDs:postID}) as Post;
    if (!post) {
        return res.status(404).end();
    }

    try {
        await APIFunctions.Posts.unrepost(req.session.accountID, postID);
    } catch (err) {
        return res.status(500).end();
    }

    res.status(204).end();
});

// gets content on logged-in user's wall
router.get('/wall', getPostsFunc);

// TODO: We don't post to someone else's wall, so don't need /users/:handle/posts

router.route('/users/:handle/posts')
// gets posts by handle
.get(getPostsFunc)
// adds post by handle
.post(async (req, res)=>{
    try {
        var accountID = await getAccountID(req.params.handle);
    } catch (err) {
        return onFailure(res, err);
    }

    if (!accountID) {
        return res.status(404).end('Handle not found');
    }

    if (accountID !== req.session.accountID) {
        let key = req.query.apikey;
        if (key) {
            try {
                var user = await DataHandler.queryDBGet(`SELECT api_key FROM accounts WHERE handle=?`, req.params.handle);
            } catch (err) {
                console.error(`Error getting user key: ${err}`);
                return res.status(500).end('Server error');
            }
        }

        if (!user || !user.api_key || user.api_key !== key) {
            return res.status(403).end('Not authorised');
        }
    }

    APIFunctions.Posts.add(req, accountID)
    .then(post=>res.json(post))
    .catch(err=>{
        if (err) {
            if (err.error) {
                if (err.error === UploadFailure.IMAGE_ISSUE) {
                    return res.status(500).end('Image corrupt or not an image');
                } else if (err.error === UploadFailure.TRANSFER_ERROR) {
                    return res.status(500).end('Error transferring image');
                } else if (err.error === UploadFailure.UNSUPPORTED_FORMAT) {
                    return res.status(400).end('File not an image or unsupported format');
                } else if (err.error === UploadFailure.UPLOAD_ABORTED) {
                    return res.status(500).end('Upload aborted');
                }
            }
        }
        onFailure(res, err);
    });
});

////////////////
///// MISC /////
////////////////

// returns users and posts for query
router.get('/search', (req, res)=>{    
    if (req.query.before) {
        var criteria = {
            time:'before',
            value:+req.query.before
        };
    } else if (req.query.after) {
        var criteria = {
            time:'after',
            value:+req.query.after
        };
    }

    APIFunctions.Search.get(req.query.query, +req.session.accountID, 20, criteria as {time:'before'|'after',value:number})
    .then(results=>res.json(results))
    .catch(err=>onFailure(res, err));
});

// gets users and tags for query
router.get('/autocomplete', (req, res)=>{
    APIFunctions.Autocomplete.get(req.query.query, req.session.accountID)
    .then(results=>res.json(results))
    .catch(err=>onFailure(res, err));
});

export default router;