/**
 * Test module for mocking fs module.
 * Based on code from https://www.w3resource.com/jest/manual-mocks.php
 */

'use strict';

const fs = jest.genMockFromModule('fs');

let files = [ ];

// sets filenames for emulated folder
function __setMockFiles(newFiles) {
    files = newFiles;
}

// overrides existsSync folder
function existsSync(path) {
    return files.includes(path);
}

// overrides unlinkSync folder
function unlinkSync(path) {
    if (!files.includes(path)) {
        throw "file doesn't exist";
    }
}

fs.__setMockFiles = __setMockFiles;
fs.existsSync = existsSync;
fs.unlinkSync = unlinkSync;

module.exports = fs;