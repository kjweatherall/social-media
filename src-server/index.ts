import * as express from 'express';
import * as session from 'express-session';
import * as path from 'path';
import * as connect from 'connect-sqlite3';

// TODO: Find a better place for this
import * as fs from 'fs';
if (!fs.existsSync(path.join(__dirname, '../data'))) {
    fs.mkdirSync(path.join(__dirname, '../data'));
}

// load settings
import Settings from './modules/Settings';
Settings.load(path.join(__dirname, '../config/settings.json'));

import Accounts from './data-helpers/modules/Accounts';

import BaseRoutes from './routes/base';
import APIRoutes from './routes/api';

import Maintainer from './helpers/Maintainer';

Maintainer();

const app = express();

app.use(express.urlencoded({extended:true}));
app.use(express.json());
app.use(express.static(path.join(__dirname, '../../public')));

// sessions
const SQLiteStore = connect(session);

let store = session({
    secret:Settings.settings.cookieSecret,
    resave:true,
    saveUninitialized:true,
    store:new SQLiteStore({
        dir:path.join(__dirname, '../data')
    }),
    cookie:{
        secure:false
    }
});

app.use(store);

// authentication
app.use((req, res, next)=>{
    if (req.session) {
        if (req.session.sessionCode) {
            // session has session code so it claims to be logged in. check this session code is valid.
            Accounts.isSessionValid(req.session.accountID, req.session.sessionCode)
            .then(isValid=>{
                if (isValid) {
                    // if session code is correct, user is logged in
                    res.locals.loggedIn = true;
                } else {
                    req.session.accountID = 0;
                    req.session.sessionCode = '';
                    res.locals.loggedIn = false;
                }

                next();
            })
            .catch(err=>{
                console.error(`Error checking session: ${err}`);
                res.status(500).end('Server error');
            });
            return;
        }

        // if account ID but no session code, clear login
        if (req.session.accountID) {
            req.session.accountID = 0;
        }
    }

    res.locals.loggedIn = false;

    next();
});

// routes
app.use('/', BaseRoutes);
app.use('/api/v1/', APIRoutes);

app.get('*', (req, res)=>res.redirect('/'));

// start server
if (!Settings.settings.subserver) {
    app.listen(Settings.settings.port)
    .on('listening', ()=>console.log(`Connected to port ${Settings.settings.port}`));
}

exports.app = app;