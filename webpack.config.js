const path = require('path');

module.exports = [
    {
        mode:"production",
        devtool:"source-map",
        entry:'./src-client',
        output:{
            filename:'main.js',
            path:path.join(__dirname, './public/dist')
        },
        resolve:{
            extensions: [".ts", ".tsx", ".js"],
            "alias":{
                "interfaces":"./shared/SharedInterface"
            }
        },
        module:{
            rules:[
                {
                    test: /\.ts(x?)$/,
                    include:[
                        path.join(__dirname, 'src-client'),
                        path.join(__dirname, 'shared')
                    ],
                    exclude: [
                        path.join(__dirname, 'node_modules'),
                        path.join(__dirname, 'src-server')
                    ],
                    use:[
                        {
                            loader: "ts-loader"
                        }
                    ]
                }
            ]
        },
        externals:{
            "react": "React",
            "react-dom": "ReactDOM",
            "socket.io-client": "io"
        }
    }
];