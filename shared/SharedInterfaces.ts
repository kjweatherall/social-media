////////////////////
///// ACCOUNTS /////
////////////////////

export type Connection = 'self'|'following'|'unconnected';

// basic account information
export interface ProfileBasic {
    displayName: string;
    handle: string;
    profileImage: string;
    profileImageThumbnailSmall: string;
    profileImageThumbnailLarge: string;
    bannerImage: string;
    bannerImageThumbnail: string;
    bio: string;
}

// full account information
export interface Profile extends ProfileBasic {
    posts: number;
    connection: Connection;
    followers: number;
    following: number;
}

export interface FollowersResults {
    followers:Profile[];
    following:Profile[];
}

/////////////////
///// POSTS /////
/////////////////

export interface Post {
    streamID: number;
    postID: number;
    displayName: string;
    profileImage: string;
    profileImageThumbnail: string;
    handle: string;
    text: string;
    publishedAt: number;
    liked?: boolean;
    likes: number;
    images?: string;
    thumbnails?: string;
    repost?: boolean;
    reposted?: boolean;
    repostedByName?: string;
    repostedByHandle?: string;
    addedAt: number;
    following: boolean;
}

export interface PostResults {
    results: Post[];
    hasMore: boolean;
}

export interface PostsQuery {
    own?: boolean;
    postIDs?: number | number[];
    accountID?: number;
    handle?: string;
    publishedAt?: {
        time: number;
        relativeToTime:'before'|'after'|'equals'
        includeTime?: boolean;
    }
    text?: {
        value: string;
        exact?: boolean;
    },
    ext?: string;
    maxResults?: number;
    imageID?: number;
    excludeReposts?: boolean;
}

export interface PostsUpdate {
    postID: number;
    publishedAt?: number;
    text?: string;
}

////////////////////
///// COMMENTS /////
////////////////////

export interface Comment {
    commentID: number;
    accountID: number;
    profileImage: string;
    postID: number;
    text: string;
    publishedAt: number;
    displayName: string;
    handle: string;
}

export interface CommentsQuery {
    commentID?: number;
    accountID?: number;
    postID?: number;
    publishedAt?:{
        time: number;
        relativeToTime: 'before'|'after'|'equals';
        includeTime: boolean;
    },
    text?:{
        value: string;
        exact?: boolean;
    },
    maxResults?: number;
    type:'count'|'basic'|'full'
}

//////////////////
///// SEARCH /////
//////////////////

export interface SearchResult {
    accountID: number;
    name: string;
    handle: string;
}

export interface SearchResults {
    profiles: Profile[];
    posts: PostResults;
}

export interface AutocompleteResults {
    accounts:ProfileBasic[];
    hashtags:string[];
}